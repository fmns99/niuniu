-- MySQL database dump
-- 主机: 
-- 生成日期: 2019 年  04 月 24 日 00:25
-- MySQL版本: 
-- PHP 版本: 5.4.45
-- 数据库: `paosjk`
-- ---------------------------------------------------------
-- 表的结构ysk_admin
--
DROP TABLE IF EXISTS `ysk_admin`;
CREATE TABLE `ysk_admin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'UID',
  `auth_id` int(11) NOT NULL DEFAULT '1' COMMENT '角色ID',
  `nickname` varchar(63) DEFAULT NULL COMMENT '昵称',
  `username` varchar(31) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(63) NOT NULL DEFAULT '' COMMENT '密码',
  `mobile` varchar(11) NOT NULL DEFAULT '' COMMENT '手机号',
  `reg_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '注册IP',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '状态',
  `reg_type` varchar(20) DEFAULT NULL COMMENT '注册人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='后台管理员表格';

--
-- 转存表中的数据 ysk_admin

INSERT INTO `ysk_admin` VALUES('1','1','超级管理员','admin','8f3bd6b4d00391c9d09cc14e32fee28c','','0','1438651748','1552141214','1','');
INSERT INTO `ysk_admin` VALUES('6','12','客服001','kefu001','ed7e774d38dad5a5a28e419e30cd8328','13000000000','1778797117','1555838776','1555839367','1','');
INSERT INTO `ysk_admin` VALUES('7','13','财务001','caiwu001','b2d90c60df6c19aafe49d98728afd024','13000000001','1778797117','1555839412','1555839412','1','');
--
-- 表的结构ysk_bankcard
--
DROP TABLE IF EXISTS `ysk_bankcard`;
CREATE TABLE `ysk_bankcard` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` int(11) NOT NULL COMMENT 'uid',
  `name` varchar(225) NOT NULL COMMENT '持卡人',
  `bankname` varchar(225) NOT NULL COMMENT '所属银行',
  `banknum` varchar(225) NOT NULL COMMENT '银行卡号',
  `addtime` varchar(225) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='银行卡管理';

--
-- 转存表中的数据 ysk_bankcard

--
-- 表的结构ysk_complaint
--
DROP TABLE IF EXISTS `ysk_complaint`;
CREATE TABLE `ysk_complaint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '投诉人id',
  `content` text CHARACTER SET utf8mb4 COMMENT '投诉内容',
  `imgs` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '图片路径',
  `status` tinyint(1) DEFAULT '0' COMMENT '0 未查看 1 已查看',
  `create_time` int(10) DEFAULT NULL COMMENT '投诉时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='投诉建议表';

--
-- 转存表中的数据 ysk_complaint

INSERT INTO `ysk_complaint` VALUES('1','1','','','0','1552122574');
--
-- 表的结构ysk_config
--
DROP TABLE IF EXISTS `ysk_config`;
CREATE TABLE `ysk_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `title` varchar(32) NOT NULL DEFAULT '' COMMENT '配置标题',
  `name` varchar(32) DEFAULT NULL COMMENT '配置名称',
  `value` text NOT NULL COMMENT '配置值',
  `group` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '配置分组',
  `type` varchar(16) NOT NULL DEFAULT '' COMMENT '配置类型',
  `options` varchar(255) NOT NULL DEFAULT '' COMMENT '配置额外值',
  `tip` varchar(100) NOT NULL DEFAULT '' COMMENT '配置说明',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `sort` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统配置表';

--
-- 转存表中的数据 ysk_config

INSERT INTO `ysk_config` VALUES('1','站点开关','TOGGLE_WEB_SITE','1','3','0','0:关闭\r\n1:开启','商城建设中......','1378898976','1406992386','1','1');
INSERT INTO `ysk_config` VALUES('2','网站标题','WEB_SITE_TITLE','','1','0','','网站标题前台显示标题','1378898976','1379235274','2','1');
INSERT INTO `ysk_config` VALUES('3','网站LOGO','WEB_SITE_LOGO','','1','0','','网站LOGO','1407003397','1407004692','3','1');
INSERT INTO `ysk_config` VALUES('4','网站描述','WEB_SITE_DESCRIPTION','','1','0','','网站搜索引擎描述','1378898976','1379235841','4','1');
INSERT INTO `ysk_config` VALUES('5','网站关键字','WEB_SITE_KEYWORD','','1','0','','网站搜索引擎关键字','1378898976','1381390100','5','1');
INSERT INTO `ysk_config` VALUES('6','版权信息','WEB_SITE_COPYRIGHT','','1','0','','设置在网站底部显示的版权信息，如“版权所有 © 2014-2015 科斯克网络科技”','1406991855','1406992583','6','1');
INSERT INTO `ysk_config` VALUES('7','网站备案号','WEB_SITE_ICP','','1','0','','设置在网站底部显示的备案号，如“苏ICP备1502009号\"','1378900335','1415983236','9','1');
INSERT INTO `ysk_config` VALUES('26','微信二维码','WEB_SITE_WX','','1','','','','0','0','0','1');
INSERT INTO `ysk_config` VALUES('32','注册开关','close_reg','1','3','','0:关闭1:开启','关闭注册功能说明','0','0','12','1');
INSERT INTO `ysk_config` VALUES('33','交易开关','close_trading','1','3','','0:关闭1:开启','交易暂时关闭，16:00后开启','0','0','13','0');
INSERT INTO `ysk_config` VALUES('41','实时价格每分钟增长','growem','','2','','','','0','0','12','1');
INSERT INTO `ysk_config` VALUES('44','奖励开关','regjifen','0','1','0','','','1407003397','1407004692','3','1');
--
-- 表的结构ysk_dj
--
DROP TABLE IF EXISTS `ysk_dj`;
CREATE TABLE `ysk_dj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `money` decimal(10,2) DEFAULT '0.00',
  `addtime` int(11) DEFAULT NULL,
  `ppid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 ysk_dj

--
-- 表的结构ysk_ewm
--
DROP TABLE IF EXISTS `ysk_ewm`;
CREATE TABLE `ysk_ewm` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '记录id',
  `uid` int(11) NOT NULL COMMENT '用户ID',
  `ewm_class` int(11) NOT NULL COMMENT '二维码类型',
  `ewm_url` varchar(225) NOT NULL COMMENT '二维码地址',
  `ewm_price` float(10,2) NOT NULL COMMENT '二维码收款金额',
  `ewm_acc` varchar(225) NOT NULL COMMENT '二维码账号',
  `uaccount` varchar(225) NOT NULL COMMENT '用户账号',
  `uname` varchar(225) NOT NULL COMMENT '用户名',
  `addtime` varchar(225) NOT NULL COMMENT '添加时间',
  `zt` int(10) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=151 DEFAULT CHARSET=utf8 COMMENT='二维码管理';

--
-- 转存表中的数据 ysk_ewm

INSERT INTO `ysk_ewm` VALUES('135','197','1','https://img.bukanba.com/ekcms/20190420/c1c883cb0076405d.png','0.00','123456','13800138000','测试','1555744379','1');
INSERT INTO `ysk_ewm` VALUES('137','197','2','https://img.bukanba.com/ekcms/20190420/e960115cd90ce6c4.jpg','0.00','13800138000','13800138000','eeeee','1555759119','1');
INSERT INTO `ysk_ewm` VALUES('138','196','2','https://img.bukanba.com/ekcms/20190421/eb75942f0899e122.jpg','0.00','zgang@vip.qq.com','18248654865','李四','1555764311','1');
INSERT INTO `ysk_ewm` VALUES('139','196','1','https://img.bukanba.com/ekcms/20190421/71fdc18309211ef7.jpg','0.00','zg274997055','18248654865','枫叶','1555784283','1');
INSERT INTO `ysk_ewm` VALUES('140','198','1','https://img.bukanba.com/ekcms/20190421/7f0c44dce9cad022.jpg','0.00','zg274997055','18248654865','张三','1555836860','1');
INSERT INTO `ysk_ewm` VALUES('141','198','2','https://img.bukanba.com/ekcms/20190422/d8342689c0d1c85c.jpg','0.00','147258369','18248654865','张三','1555836917','1');
INSERT INTO `ysk_ewm` VALUES('142','197','2','https://img.bukanba.com/ekcms/20190421/7c5f5c7ded0ecc74.jpg','0.00','123456','13800138000','斑马','1555855557','1');
INSERT INTO `ysk_ewm` VALUES('143','199','2','https://img.bukanba.com/ekcms/20190422/7720a37ffaa7cdc9.jpg','0.00','147258369','18248654865','张跑','1555941404','1');
INSERT INTO `ysk_ewm` VALUES('145','200','1','https://img.bukanba.com/ekcms/20190422/50f2c2820167f0e4.jpg','0.00','2583','13222222222','2580','1555943500','1');
INSERT INTO `ysk_ewm` VALUES('146','200','2','https://img.bukanba.com/ekcms/20190422/eb75942f0899e122.jpg','0.00','147258','13222222222','147258','1555943627','1');
INSERT INTO `ysk_ewm` VALUES('147','201','1','https://img.bukanba.com/ekcms/20190423/9d8fa9c29d424b75.jpg','0.00','147258369','13000000000','张三','1555953566','1');
INSERT INTO `ysk_ewm` VALUES('148','201','2','https://img.bukanba.com/ekcms/20190423/8f460fc520bf262b.jpg','0.00','147258369','13000000000','张三','1555953593','1');
INSERT INTO `ysk_ewm` VALUES('149','202','2','https://img.bukanba.com/ekcms/20190423/fd27068c02e0588e.jpeg','0.00','15279770118','15279770118','陈鹏','1555954717','1');
INSERT INTO `ysk_ewm` VALUES('150','205','2','https://img.bukanba.com/ekcms/20190423/3ee8107024a81dd8.jpeg','0.00','122@qq.com','18000000000','123','1556030443','1');
--
-- 表的结构ysk_group
--
DROP TABLE IF EXISTS `ysk_group`;
CREATE TABLE `ysk_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '部门ID',
  `pid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上级部门ID',
  `title` varchar(31) NOT NULL DEFAULT '' COMMENT '部门名称',
  `icon` varchar(31) NOT NULL DEFAULT '' COMMENT '图标',
  `menu_auth` text NOT NULL COMMENT '权限列表',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '修改时间',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序（同级有效）',
  `status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '状态',
  `auth_id` int(11) DEFAULT NULL,
  `hylb` varchar(10) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='部门信息表';

--
-- 转存表中的数据 ysk_group

INSERT INTO `ysk_group` VALUES('1','0','超级管理员','','','1426881003','1427552428','0','1','1','0');
INSERT INTO `ysk_group` VALUES('2','0','财务查看','','1,7,8,9,337,10,11,316,341,340,344,324,342,322,338,3,323,347','1498324367','1551095515','0','-1','2','5');
INSERT INTO `ysk_group` VALUES('7','0','超级管理','','1,3,4,6,327,7,8,9,316,318,322,323','1526152893','1528963727','0','-1','0','');
INSERT INTO `ysk_group` VALUES('8','0','数据管理','','1,3,4,327,7,8,10,11,315,324,325,334,329,328','1527085184','1527140823','0','-1','0','0');
INSERT INTO `ysk_group` VALUES('9','0','财务','','349,351,359,323','1555013895','1555013895','1','-1','','2');
INSERT INTO `ysk_group` VALUES('10','0','客服','','354,355,356','1555200785','1555322515','1','-1','','');
INSERT INTO `ysk_group` VALUES('12','0','客服001','','352,354,355,356','1555838694','1555839116','5','1','','1,2,3,4,5');
INSERT INTO `ysk_group` VALUES('13','0','财务001','','7,348,349,351','1555839336','1555839336','6','1','','');
--
-- 表的结构ysk_menu
--
DROP TABLE IF EXISTS `ysk_menu`;
CREATE TABLE `ysk_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '菜单名称',
  `pid` int(11) NOT NULL COMMENT '父级id',
  `gid` int(11) NOT NULL DEFAULT '0' COMMENT '爷爷ID、',
  `col` varchar(30) NOT NULL COMMENT '控制器',
  `act` varchar(30) NOT NULL COMMENT '方法',
  `patch` varchar(50) DEFAULT NULL COMMENT '全路径',
  `level` int(11) NOT NULL COMMENT '级别',
  `icon` varchar(50) DEFAULT NULL,
  `sort` char(4) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=556 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 ysk_menu

INSERT INTO `ysk_menu` VALUES('1','系统','0','0','','','','0','fa-cog','0','1');
INSERT INTO `ysk_menu` VALUES('3','统用功能','1','1','','','','1','fa-folder-open-o','3','1');
INSERT INTO `ysk_menu` VALUES('5','角色管理','3','1','Group','index','','2','fa-sitemap','12','1');
INSERT INTO `ysk_menu` VALUES('6','管理员管理','3','1','Manage','index','','2','fa fa-cog','13','1');
INSERT INTO `ysk_menu` VALUES('7','会员管理','1','1','','','','1','fa-folder-open-o','1','1');
INSERT INTO `ysk_menu` VALUES('8','会员列表','7','1','User','index','','2','fa-user','21','1');
INSERT INTO `ysk_menu` VALUES('9','推荐结构','7','1','Tree','index','','2','fa-th-large','22','1');
INSERT INTO `ysk_menu` VALUES('323','系统公告','3','1','News','index','','2','fa-twitter-square','51','1');
INSERT INTO `ysk_menu` VALUES('327','数据库管理','3','1','Database','index','','2','fa fa-lock','14','1');
INSERT INTO `ysk_menu` VALUES('348','充值管理','7','1','User','recharge','','2','fa-file-text','34','1');
INSERT INTO `ysk_menu` VALUES('349','提现管理','7','1','User','withdraw','','2','fa-file-text','35','1');
INSERT INTO `ysk_menu` VALUES('350','二维码管理','7','1','User','ewm','','2','fa-file-text','36','1');
INSERT INTO `ysk_menu` VALUES('351','银行卡管理','7','1','User','bankcard','','2','fa-file-text','37','1');
INSERT INTO `ysk_menu` VALUES('352','抢单管理','1','1','','','','1','fa-folder-open-o','2','1');
INSERT INTO `ysk_menu` VALUES('353','发布订单列表','352','1','Roborder','index','','2','fa-user','38','1');
INSERT INTO `ysk_menu` VALUES('354','会员抢单列表','352','1','Roborder','userrob','','2','fa-file-text','39','1');
INSERT INTO `ysk_menu` VALUES('355','匹配成功列表','352','1','Roborder','robsucc','','2','fa-file-text','40','1');
INSERT INTO `ysk_menu` VALUES('356','交易成功列表','352','1','Roborder','ordersucc','','2','fa-file-text','41','1');
INSERT INTO `ysk_menu` VALUES('357','游戏参数设置','352','1','Roborder','asystem','','2','fa-file-text','43','1');
INSERT INTO `ysk_menu` VALUES('358','收款二维码管理','3','1','Roborder','skewm','','2','fa-twitter-square','42','0');
INSERT INTO `ysk_menu` VALUES('359','资金流水','7','1','User','bill','','2','fa-file-text','43','1');
INSERT INTO `ysk_menu` VALUES('400','代理列表','7','1','User','indexs','','2','fa-user','11','1');
INSERT INTO `ysk_menu` VALUES('401','增加代理','7','1','User','adds','','2','fa-user','10','1');
INSERT INTO `ysk_menu` VALUES('402','密码修改','3','1','admin/Manage','edit/id/1','','2','fa-folder-open-o','11','1');
INSERT INTO `ysk_menu` VALUES('555','测试一键清理','7','1','Database','index','','2','fa fa-lock','14','1');
--
-- 表的结构ysk_news
--
DROP TABLE IF EXISTS `ysk_news`;
CREATE TABLE `ysk_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '文章标题',
  `image` varchar(255) NOT NULL DEFAULT '' COMMENT '文章图片',
  `sort` smallint(6) NOT NULL DEFAULT '0',
  `desc` varchar(255) NOT NULL DEFAULT '',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `is_out` tinyint(4) NOT NULL DEFAULT '0',
  `content` text NOT NULL COMMENT '内容',
  `from` varchar(255) NOT NULL DEFAULT '' COMMENT '文章来源',
  `visit` smallint(6) NOT NULL DEFAULT '0',
  `lang` tinyint(4) NOT NULL DEFAULT '0',
  `tuijian` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统公告';

--
-- 转存表中的数据 ysk_news

INSERT INTO `ysk_news` VALUES('4','李春','','0','','1555009963','0','建设银行卡卡号12345867382922piu','','0','0','0');
--
-- 表的结构ysk_notice
--
DROP TABLE IF EXISTS `ysk_notice`;
CREATE TABLE `ysk_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notice_tittle` varchar(80) NOT NULL COMMENT '公告标题',
  `notice_content` varchar(600) NOT NULL COMMENT '公告详情',
  `notice_addtime` varchar(20) NOT NULL COMMENT '公告添加时间',
  `notice_read` text NOT NULL COMMENT '看过公告会员',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 ysk_notice

--
-- 表的结构ysk_qrcode
--
DROP TABLE IF EXISTS `ysk_qrcode`;
CREATE TABLE `ysk_qrcode` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` int(11) NOT NULL COMMENT '会员ID',
  `uname` varchar(225) NOT NULL COMMENT '会员名称',
  `code_class` int(2) NOT NULL COMMENT '二维码类型1支付宝2微信3银行卡',
  `code_url` varchar(225) NOT NULL COMMENT '二维码图片地址',
  `uaccount` varchar(225) NOT NULL COMMENT '会员账号',
  `code_acc` varchar(225) NOT NULL COMMENT '二维码账号，如支付宝账号',
  `addtime` varchar(225) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='二维码管理';

--
-- 转存表中的数据 ysk_qrcode

--
-- 表的结构ysk_recharge
--
DROP TABLE IF EXISTS `ysk_recharge`;
CREATE TABLE `ysk_recharge` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` int(11) NOT NULL COMMENT '会员ID',
  `account` varchar(225) NOT NULL COMMENT '会员账号',
  `name` varchar(225) NOT NULL COMMENT '姓名',
  `price` float(10,2) NOT NULL COMMENT '充值金额',
  `way` int(11) NOT NULL COMMENT '充值方式：1支付宝2微信3银行卡',
  `addtime` varchar(225) NOT NULL COMMENT '充值日期',
  `status` int(11) NOT NULL COMMENT '充值状态1提交，2退回，3成功',
  `marker` varchar(225) NOT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COMMENT='会员充值表';

--
-- 转存表中的数据 ysk_recharge

INSERT INTO `ysk_recharge` VALUES('51','196','18248654865','张三','1000.00','3','1555725042','3','');
INSERT INTO `ysk_recharge` VALUES('52','196','18248654865','张三','3000.00','3','1555749472','2','');
INSERT INTO `ysk_recharge` VALUES('53','198','18248654865','张三','1000.00','3','1555836645','3','');
INSERT INTO `ysk_recharge` VALUES('54','198','18248654865','张三','10000.00','3','1555836702','3','');
INSERT INTO `ysk_recharge` VALUES('55','201','13000000000','李四','50000.00','3','1555953506','3','');
INSERT INTO `ysk_recharge` VALUES('56','199','18248654865','张三','10000.00','3','1555985977','3','');
--
-- 表的结构ysk_roborder
--
DROP TABLE IF EXISTS `ysk_roborder`;
CREATE TABLE `ysk_roborder` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `class` int(2) NOT NULL COMMENT '收款类型',
  `price` float(10,2) NOT NULL COMMENT '收款金额',
  `addtime` varchar(225) NOT NULL COMMENT '添加时间',
  `status` int(2) NOT NULL COMMENT '订单状态',
  `uid` int(11) NOT NULL COMMENT '匹配用户ID',
  `uname` varchar(225) NOT NULL COMMENT '匹配用户名称',
  `umoney` float(10,2) NOT NULL COMMENT '匹配用户余额',
  `pipeitime` varchar(225) NOT NULL COMMENT '匹配时间',
  `finishtime` varchar(225) NOT NULL COMMENT '完成时间',
  `ordernum` varchar(225) NOT NULL COMMENT '订单号',
  `class2` int(11) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1006 DEFAULT CHARSET=utf8 COMMENT='抢单表';

--
-- 转存表中的数据 ysk_roborder

INSERT INTO `ysk_roborder` VALUES('758','2','10.00','1555913822','3','197','测试','18855.00','1555913884','','E15559138199481','','182.122.101.153');
INSERT INTO `ysk_roborder` VALUES('759','2','10.00','1555913893','3','197','测试','18845.10','1555914097','','E15559138868867','','182.122.101.153');
INSERT INTO `ysk_roborder` VALUES('760','1','10.00','1555914159','2','197','测试','18845.20','1555914198','','E15559141016589','','182.122.101.153');
INSERT INTO `ysk_roborder` VALUES('761','2','10.00','1555914807','2','197','测试','18835.20','1555914829','','E1555914803191','','182.122.101.153');
INSERT INTO `ysk_roborder` VALUES('762','1','10.00','1555914949','2','197','测试','18825.20','1555914949','','E15559149465012','','182.122.101.153');
INSERT INTO `ysk_roborder` VALUES('763','1','10.00','1555914980','3','197','测试','18805.20','1555915024','','E15559149792943','','182.122.101.153');
INSERT INTO `ysk_roborder` VALUES('764','1','10.00','1555915065','2','197','测试','18805.32','1555915065','','E15559150615086','','182.122.101.153');
INSERT INTO `ysk_roborder` VALUES('765','2','10.00','1555916307','2','197','测试','18805.32','1555916320','','E15559163059495','','182.122.101.153');
INSERT INTO `ysk_roborder` VALUES('767','2','10.00','1555916478','3','197','测试','17950.46','1555916966','','E15559164755339','','182.122.101.153');
INSERT INTO `ysk_roborder` VALUES('768','1','10.00','1555916534','3','197','测试','17950.34','1555916942','','E1555916532802','','182.122.101.153');
INSERT INTO `ysk_roborder` VALUES('769','1','169.00','1555916626','3','197','测试','17948.31','1555916930','','E1555916624451','','182.122.101.153');
INSERT INTO `ysk_roborder` VALUES('770','1','666.00','1555916845','3','197','测试','17940.32','1555916905','','E15559168436086','','182.122.101.153');
INSERT INTO `ysk_roborder` VALUES('771','1','10.00','1555917023','2','197','测试','17950.56','1555917029','','E15559170161260','','182.122.101.153');
INSERT INTO `ysk_roborder` VALUES('772','2','10.00','1555917175','2','197','测试','17940.56','1555917194','','E1555917172825','','182.122.101.153');
INSERT INTO `ysk_roborder` VALUES('773','1','998.00','1555917431','3','197','测试','16942.56','1555917583','','E15559171798750','','182.122.101.153');
INSERT INTO `ysk_roborder` VALUES('774','1','369.00','1555917625','2','197','测试','16954.54','1555917631','','E15559176211589','','182.122.101.153');
INSERT INTO `ysk_roborder` VALUES('775','2','800.00','1555917702','2','197','测试','16585.54','1555917712','','E15559177002803','','182.122.101.153');
INSERT INTO `ysk_roborder` VALUES('776','2','120.00','1555918681','3','198','张三','10515.00','1555918717','','E15559186782164','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('777','2','153.00','1555918736','3','198','张三','10177.08','1555918873','','E15559187337698','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('778','2','188.00','1555918813','3','198','张三','10175.20','1555918860','','E15559188071621','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('779','2','188.00','1555918888','3','198','张三','9791.61','1555919022','','E15559188862805','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('780','2','199.00','1555918952','3','198','张三','9793.49','1555919038','','E15559189504686','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('781','2','233.00','1555919076','3','198','张三','9562.48','1555919113','','E15559190686551','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('782','1','244.00','1555919172','3','198','张三','9320.81','1555919195','','E15559191658798','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('783','1','145.00','1555919215','3','198','张三','9178.74','1555919279','','E15559192128540','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('785','2','250.00','1555925780','3','198','张三','8930.48','1555925798','','E15559257778617','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('786','2','230.00','1555925827','3','198','张三','8702.98','1555925861','','E15559258202747','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('787','2','230.00','1555925898','3','198','张三','8475.28','1555926076','','E15559258959496','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('790','2','1000.00','1555926210','3','198','张三','7477.58','1555926252','','E15559262042853','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('791','1','1000.00','1555926352','3','198','张三','6487.58','1555926405','','E15559263455735','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('792','2','999.00','1555926426','3','198','张三','5500.58','1555926443','','E15559264237513','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('793','2','898.00','1555926481','3','198','张三','4612.57','1555926501','','E15559264731081','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('795','2','132.00','1555926661','3','198','张三','4489.55','1555926738','','E15559266391608','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('797','2','100.00','1555926942','3','198','张三','4278.87','1555927188','','E15559269396917','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('798','1','112.00','1555927131','3','198','张三','4279.87','1555927203','','E15559271241414','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('800','2','100.00','1555927303','2','198','张三','4281.21','1555927304','','E15559273004815','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('801','2','50.00','1555927382','2','198','张三','4181.21','1555927383','','E15559273797124','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('802','2','124.00','1555927593','3','198','张三','3560.49','1555928461','','E15559275869616','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('803','1','123.00','1555927750','3','198','张三','3559.01','1555928449','','E15559277471828','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('806','2','258.00','1555928289','3','198','张三','3726.21','1555928372','','E15559282818469','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('807','2','222.00','1555928342','3','198','张三','3556.79','1555928421','','E15559282877634','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('813','2','200.00','1555941419','3','199','左刚','8453.60','1555942228','','E15559414189577','','117.136.124.174');
INSERT INTO `ysk_roborder` VALUES('814','2','200.00','1555941458','3','199','左刚','8440.00','1555942158','','E15559414563488','','117.136.124.174');
INSERT INTO `ysk_roborder` VALUES('815','2','300.00','1555941637','3','199','左刚','8450.60','1555942216','','E15559416349013','','117.136.124.174');
INSERT INTO `ysk_roborder` VALUES('816','2','500.00','1555941847','3','199','左刚','8445.60','1555942198','','E15559418457674','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('817','2','360.00','1555942002','3','199','左刚','8442.00','1555942171','','E15559420001678','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('818','2','50.00','1555942206','3','199','左刚','8405.60','1555942302','','E15559422043148','','182.122.101.153');
INSERT INTO `ysk_roborder` VALUES('819','2','134.00','1555942457','2','199','左刚','8406.10','1555942467','','E1555942454687','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('820','2','241.00','1555942549','3','199','左刚','7581.10','1555942735','','E1555942547467','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('821','2','205.00','1555942609','2','199','左刚','8031.10','1555942611','','E15559426074132','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('823','2','245.00','1555942715','2','199','左刚','7826.10','1555942716','','E15559427139260','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('827','2','130.00','1555943636','3','200','','9870.00','1555943659','','E15559436333018','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('828','2','260.00','1555946173','2','199','左刚','7828.51','1555946175','','E15559461709357','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('829','2','222.00','1555946417','2','199','左刚','7568.51','1555946424','','E15559464151514','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('830','2','444.00','1555946455','2','199','左刚','7346.51','1555946456','','E1555946452853','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('831','2','234.00','1555946477','2','199','左刚','6902.51','1555946486','','E15559464756315','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('832','2','234.00','1555946508','2','199','左刚','6668.51','1555946520','','E15559465062502','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('833','2','212.00','1555946550','2','199','左刚','6434.51','1555946552','','E15559465489442','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('834','2','10.00','1555947529','2','199','左刚','6222.51','1555947529','','E15559475261326','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('835','2','258.00','1555948746','3','199','左刚','5964.51','1555948818','','E15559487435679','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('836','2','258.00','1555948874','2','199','左刚','5967.09','1555948883','','E15559488669256','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('837','2','25.00','1555951417','2','199','左刚','5967.09','1555951419','','E15559514136241','','117.136.124.19');
INSERT INTO `ysk_roborder` VALUES('838','2','150.00','1555953635','3','201','网络','49850.00','1555953763','','E1555953633122','','117.136.124.19');
INSERT INTO `ysk_roborder` VALUES('839','2','369.00','1555954022','3','201','网络','49482.50','1555954051','','E155595401486','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('840','2','20000.00','1555954771','3','202','陈鹏','80000.00','1555954788','','E15559547696534','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('841','2','78000.00','1555954883','3','202','陈鹏','2200.00','1555954896','','E1555954878880','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('842','2','50000.00','1555955004','3','202','陈鹏','50000.00','1555955020','','E15559549983071','','106.6.74.61');
INSERT INTO `ysk_roborder` VALUES('906','2','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('844','2','258.00','1555985782','3','199','左刚','9767.00','1555986236','','E1555985779277','','117.136.124.13');
INSERT INTO `ysk_roborder` VALUES('845','2','1055.00','1555986278','2','199','左刚','9769.58','1555986291','','E15559862747908','','117.136.124.13');
INSERT INTO `ysk_roborder` VALUES('885','2','0.00','1556003084','3','199','左刚','9682.46','1556019399','','N899539353844','','');
INSERT INTO `ysk_roborder` VALUES('890','2','88.00','1556015680','3','199','左刚','9681.58','1556019339','','E15560156788727','','106.6.73.8');
INSERT INTO `ysk_roborder` VALUES('898','2','258.00','1556031436','3','199','左刚','9337.00','1556031463','','E15560314331707','','106.6.73.8');
INSERT INTO `ysk_roborder` VALUES('897','2','99.00','1556031233','3','199','左刚','9594.01','1556031327','','E15560312313440','','106.6.73.8');
INSERT INTO `ysk_roborder` VALUES('899','2','253.00','1556031488','2','199','左刚','9339.58','1556031488','','E15560314807078','','106.6.73.8');
INSERT INTO `ysk_roborder` VALUES('900','2','258.00','1556032042','3','199','左刚','9081.58','1556032496','','E15560320408577','','106.6.73.8');
INSERT INTO `ysk_roborder` VALUES('907','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('905','2','0.00','1556034920','2','199','左刚','9086.58','1556034920','','N840729609997','','');
INSERT INTO `ysk_roborder` VALUES('908','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('909','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('910','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('911','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('912','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('913','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('914','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('915','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('916','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('917','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('918','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('919','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('920','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('921','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('922','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('923','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('924','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('925','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('926','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('927','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('928','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('929','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('930','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('931','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('932','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('933','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('934','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('935','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('936','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('937','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('938','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('939','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('940','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('941','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('942','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('943','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('944','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('945','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('946','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('947','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('948','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('949','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('950','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('951','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('952','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('953','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('954','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('955','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('956','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('957','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('958','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('959','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('960','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('961','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('962','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('963','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('964','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('965','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('966','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('967','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('968','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('969','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('970','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('971','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('972','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('973','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('974','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('975','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('976','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('977','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('978','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('979','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('980','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('981','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('982','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('983','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('984','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('985','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('986','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('987','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('988','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('989','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('990','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('991','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('992','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('993','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('994','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('995','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('996','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('997','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('998','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('999','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('1000','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('1001','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('1002','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('1003','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('1004','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
INSERT INTO `ysk_roborder` VALUES('1005','1','100000.00','1556036086','1','0','','0.00','','','N010705974551','','');
--
-- 表的结构ysk_skm
--
DROP TABLE IF EXISTS `ysk_skm`;
CREATE TABLE `ysk_skm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wxewm` varchar(225) NOT NULL,
  `zfbewm` varchar(225) NOT NULL,
  `bankewm` varchar(225) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='收款码';

--
-- 转存表中的数据 ysk_skm

INSERT INTO `ysk_skm` VALUES('1','2019pay/2019-03-20/5c911c22156dc.png','2019pay/2019-03-20/5c911c22188b8.png','2019pay/2019-03-20/5c911c221b2c7.png');
--
-- 表的结构ysk_somebill
--
DROP TABLE IF EXISTS `ysk_somebill`;
CREATE TABLE `ysk_somebill` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` int(11) NOT NULL COMMENT '会员ID',
  `jl_class` int(11) NOT NULL DEFAULT '0' COMMENT '流水类别：1佣金2团队奖励3充值4提现5订单匹配 6 BC充值',
  `info` varchar(225) NOT NULL COMMENT '说明',
  `addtime` varchar(225) NOT NULL COMMENT '事件时间',
  `jc_class` varchar(225) NOT NULL COMMENT '分+ 或-',
  `num` float(10,2) NOT NULL COMMENT '币量',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=708 DEFAULT CHARSET=utf8 COMMENT='会员流水账单';

--
-- 转存表中的数据 ysk_somebill

INSERT INTO `ysk_somebill` VALUES('290','176','6','充值100.00确认-','1555651957','-','100.00');
INSERT INTO `ysk_somebill` VALUES('291','176','6','佣金收入+','1555651957','+','1.40');
INSERT INTO `ysk_somebill` VALUES('292','176','5','充值100.00确认-','1555651957','-','100.00');
INSERT INTO `ysk_somebill` VALUES('293','176','6','充值100.00确认-','1555651973','-','100.00');
INSERT INTO `ysk_somebill` VALUES('294','176','6','佣金收入+','1555651973','+','1.40');
INSERT INTO `ysk_somebill` VALUES('295','176','5','充值100.00确认-','1555651973','-','100.00');
INSERT INTO `ysk_somebill` VALUES('296','185','6','充值100.00确认-','1555654336','-','100.00');
INSERT INTO `ysk_somebill` VALUES('297','185','6','佣金收入+','1555654336','+','1.40');
INSERT INTO `ysk_somebill` VALUES('298','185','5','充值100.00确认-','1555654336','-','100.00');
INSERT INTO `ysk_somebill` VALUES('299','185','6','充值1000.00确认-','1555655766','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('300','185','6','佣金收入+','1555655766','+','14.00');
INSERT INTO `ysk_somebill` VALUES('301','185','5','充值1000.00确认-','1555655766','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('302','185','6','充值10000.00确认-','1555655827','-','10000.00');
INSERT INTO `ysk_somebill` VALUES('303','185','6','佣金收入+','1555655827','+','140.00');
INSERT INTO `ysk_somebill` VALUES('304','185','5','充值10000.00确认-','1555655827','-','10000.00');
INSERT INTO `ysk_somebill` VALUES('305','185','6','充值1000.00确认-','1555656311','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('306','185','6','佣金收入+','1555656311','+','14.00');
INSERT INTO `ysk_somebill` VALUES('307','185','5','充值1000.00确认-','1555656311','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('308','185','6','充值1000.00确认-','1555658107','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('309','185','6','佣金收入+','1555658107','+','14.00');
INSERT INTO `ysk_somebill` VALUES('310','185','5','充值1000.00确认-','1555658107','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('311','185','6','充值1000.00确认-','1555658141','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('312','185','6','佣金收入+','1555658141','+','14.00');
INSERT INTO `ysk_somebill` VALUES('313','185','5','充值1000.00确认-','1555658141','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('314','186','6','充值10000.00确认-','1555658890','-','10000.00');
INSERT INTO `ysk_somebill` VALUES('315','186','6','佣金收入+','1555658890','+','100.00');
INSERT INTO `ysk_somebill` VALUES('316','186','5','充值10000.00确认-','1555658890','-','10000.00');
INSERT INTO `ysk_somebill` VALUES('317','186','6','充值1000000.00确认-','1555660230','-','1000000.00');
INSERT INTO `ysk_somebill` VALUES('318','186','6','佣金收入+','1555660230','+','10000.00');
INSERT INTO `ysk_somebill` VALUES('319','186','5','充值1000000.00确认-','1555660230','-','1000000.00');
INSERT INTO `ysk_somebill` VALUES('320','186','6','充值4444.00确认-','1555660739','-','4444.00');
INSERT INTO `ysk_somebill` VALUES('321','186','6','佣金收入+','1555660739','+','44.44');
INSERT INTO `ysk_somebill` VALUES('322','186','5','充值4444.00确认-','1555660739','-','4444.00');
INSERT INTO `ysk_somebill` VALUES('323','186','6','充值10000.00确认-','1555660924','-','10000.00');
INSERT INTO `ysk_somebill` VALUES('324','186','6','佣金收入+','1555660924','+','100.00');
INSERT INTO `ysk_somebill` VALUES('325','186','5','充值10000.00确认-','1555660924','-','10000.00');
INSERT INTO `ysk_somebill` VALUES('326','186','6','充值1999.00确认-','1555661047','-','1999.00');
INSERT INTO `ysk_somebill` VALUES('327','186','6','佣金收入+','1555661047','+','19.99');
INSERT INTO `ysk_somebill` VALUES('328','186','5','充值1999.00确认-','1555661047','-','1999.00');
INSERT INTO `ysk_somebill` VALUES('329','186','6','充值987.00确认-','1555661126','-','987.00');
INSERT INTO `ysk_somebill` VALUES('330','186','6','佣金收入+','1555661126','+','9.87');
INSERT INTO `ysk_somebill` VALUES('331','186','5','充值987.00确认-','1555661126','-','987.00');
INSERT INTO `ysk_somebill` VALUES('332','186','6','充值1234.00确认-','1555661171','-','1234.00');
INSERT INTO `ysk_somebill` VALUES('333','186','6','佣金收入+','1555661171','+','12.34');
INSERT INTO `ysk_somebill` VALUES('334','186','5','充值1234.00确认-','1555661171','-','1234.00');
INSERT INTO `ysk_somebill` VALUES('335','185','6','充值100.00确认-','1555662016','-','100.00');
INSERT INTO `ysk_somebill` VALUES('336','185','6','佣金收入+','1555662016','+','1.00');
INSERT INTO `ysk_somebill` VALUES('337','185','5','充值100.00确认-','1555662016','-','100.00');
INSERT INTO `ysk_somebill` VALUES('338','185','6','充值100.00确认-','1555662044','-','100.00');
INSERT INTO `ysk_somebill` VALUES('339','185','6','佣金收入+','1555662044','+','1.00');
INSERT INTO `ysk_somebill` VALUES('340','185','5','充值100.00确认-','1555662044','-','100.00');
INSERT INTO `ysk_somebill` VALUES('341','185','6','充值100.00确认-','1555662080','-','100.00');
INSERT INTO `ysk_somebill` VALUES('342','185','6','佣金收入+','1555662080','+','1.00');
INSERT INTO `ysk_somebill` VALUES('343','185','5','充值100.00确认-','1555662080','-','100.00');
INSERT INTO `ysk_somebill` VALUES('344','185','6','充值100.00确认-','1555663278','-','100.00');
INSERT INTO `ysk_somebill` VALUES('345','185','6','佣金收入+','1555663278','+','1.00');
INSERT INTO `ysk_somebill` VALUES('346','185','5','充值100.00确认-','1555663278','-','100.00');
INSERT INTO `ysk_somebill` VALUES('347','185','6','充值10.00确认-','1555663344','-','10.00');
INSERT INTO `ysk_somebill` VALUES('348','185','6','佣金收入+','1555663344','+','0.10');
INSERT INTO `ysk_somebill` VALUES('349','185','5','充值10.00确认-','1555663344','-','10.00');
INSERT INTO `ysk_somebill` VALUES('350','185','6','充值100.00确认-','1555663582','-','100.00');
INSERT INTO `ysk_somebill` VALUES('351','185','6','佣金收入+','1555663582','+','1.00');
INSERT INTO `ysk_somebill` VALUES('352','185','5','充值100.00确认-','1555663582','-','100.00');
INSERT INTO `ysk_somebill` VALUES('353','185','6','充值10000.00确认-','1555664868','-','10000.00');
INSERT INTO `ysk_somebill` VALUES('354','185','6','佣金收入+','1555664868','+','100.00');
INSERT INTO `ysk_somebill` VALUES('355','185','5','充值10000.00确认-','1555664868','-','10000.00');
INSERT INTO `ysk_somebill` VALUES('356','185','6','充值1000.00确认-','1555664882','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('357','185','6','佣金收入+','1555664882','+','10.00');
INSERT INTO `ysk_somebill` VALUES('358','185','5','充值1000.00确认-','1555664882','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('359','185','6','充值1000.00确认-','1555664896','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('360','185','6','佣金收入+','1555664896','+','10.00');
INSERT INTO `ysk_somebill` VALUES('361','185','5','充值1000.00确认-','1555664896','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('362','185','6','充值10000.00确认-','1555664909','-','10000.00');
INSERT INTO `ysk_somebill` VALUES('363','185','6','佣金收入+','1555664909','+','100.00');
INSERT INTO `ysk_somebill` VALUES('364','185','5','充值10000.00确认-','1555664909','-','10000.00');
INSERT INTO `ysk_somebill` VALUES('365','185','6','充值100.00确认-','1555664927','-','100.00');
INSERT INTO `ysk_somebill` VALUES('366','185','6','佣金收入+','1555664927','+','1.00');
INSERT INTO `ysk_somebill` VALUES('367','185','5','充值100.00确认-','1555664927','-','100.00');
INSERT INTO `ysk_somebill` VALUES('368','185','6','充值100.00确认-','1555664938','-','100.00');
INSERT INTO `ysk_somebill` VALUES('369','185','6','佣金收入+','1555664938','+','1.00');
INSERT INTO `ysk_somebill` VALUES('370','185','5','充值100.00确认-','1555664938','-','100.00');
INSERT INTO `ysk_somebill` VALUES('371','185','6','充值1000.00确认-','1555664953','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('372','185','6','佣金收入+','1555664953','+','10.00');
INSERT INTO `ysk_somebill` VALUES('373','185','5','充值1000.00确认-','1555664953','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('374','185','6','充值10.00确认-','1555665220','-','10.00');
INSERT INTO `ysk_somebill` VALUES('375','185','6','佣金收入+','1555665220','+','0.10');
INSERT INTO `ysk_somebill` VALUES('376','185','5','充值10.00确认-','1555665220','-','10.00');
INSERT INTO `ysk_somebill` VALUES('377','185','6','充值1000.00确认-','1555665364','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('378','185','6','佣金收入+','1555665364','+','10.00');
INSERT INTO `ysk_somebill` VALUES('379','185','5','充值1000.00确认-','1555665364','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('380','185','6','充值10.00确认-','1555682589','-','10.00');
INSERT INTO `ysk_somebill` VALUES('381','185','6','佣金收入+','1555682589','+','0.10');
INSERT INTO `ysk_somebill` VALUES('382','185','5','充值10.00确认-','1555682589','-','10.00');
INSERT INTO `ysk_somebill` VALUES('383','185','6','充值100.00确认-','1555684144','-','100.00');
INSERT INTO `ysk_somebill` VALUES('384','185','6','佣金收入+','1555684144','+','1.00');
INSERT INTO `ysk_somebill` VALUES('385','185','5','充值100.00确认-','1555684144','-','100.00');
INSERT INTO `ysk_somebill` VALUES('386','185','6','充值1000.00确认-','1555684191','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('387','185','6','佣金收入+','1555684191','+','10.00');
INSERT INTO `ysk_somebill` VALUES('388','185','5','充值1000.00确认-','1555684191','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('389','185','6','充值1000.00确认-','1555684490','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('390','185','6','佣金收入+','1555684490','+','10.00');
INSERT INTO `ysk_somebill` VALUES('391','185','5','充值1000.00确认-','1555684490','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('392','185','6','充值1000.00确认-','1555684572','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('393','185','6','佣金收入+','1555684572','+','10.00');
INSERT INTO `ysk_somebill` VALUES('394','185','5','充值1000.00确认-','1555684572','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('395','184','6','充值10.00确认-','1555685146','-','10.00');
INSERT INTO `ysk_somebill` VALUES('396','184','6','佣金收入+','1555685146','+','0.10');
INSERT INTO `ysk_somebill` VALUES('397','184','5','充值10.00确认-','1555685146','-','10.00');
INSERT INTO `ysk_somebill` VALUES('398','190','6','充值1000.00确认-','1555686904','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('399','190','6','佣金收入+','1555686904','+','10.00');
INSERT INTO `ysk_somebill` VALUES('400','190','5','充值1000.00确认-','1555686904','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('401','193','6','充值1000.00确认-','1555688173','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('402','193','6','佣金收入+','1555688173','+','10.00');
INSERT INTO `ysk_somebill` VALUES('403','193','5','充值1000.00确认-','1555688173','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('404','189','6','充值1000.00确认-','1555692906','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('405','189','6','佣金收入+','1555692906','+','10.00');
INSERT INTO `ysk_somebill` VALUES('406','189','5','充值1000.00确认-','1555692906','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('407','189','6','充值100.00确认-','1555724149','-','100.00');
INSERT INTO `ysk_somebill` VALUES('408','189','6','佣金收入+','1555724149','+','1.00');
INSERT INTO `ysk_somebill` VALUES('409','189','5','充值100.00确认-','1555724149','-','100.00');
INSERT INTO `ysk_somebill` VALUES('410','196','6','充值10.00确认-','1555725697','-','10.00');
INSERT INTO `ysk_somebill` VALUES('411','196','6','佣金收入+','1555725697','+','0.10');
INSERT INTO `ysk_somebill` VALUES('412','196','5','充值10.00确认-','1555725697','-','10.00');
INSERT INTO `ysk_somebill` VALUES('413','196','6','充值1000.00确认-','1555725979','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('414','196','6','佣金收入+','1555725979','+','10.00');
INSERT INTO `ysk_somebill` VALUES('415','196','5','充值1000.00确认-','1555725979','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('416','196','6','充值500.00确认-','1555726314','-','500.00');
INSERT INTO `ysk_somebill` VALUES('417','196','6','佣金收入+','1555726314','+','5.00');
INSERT INTO `ysk_somebill` VALUES('418','196','5','充值500.00确认-','1555726314','-','500.00');
INSERT INTO `ysk_somebill` VALUES('419','196','6','充值2000.00确认-','1555726365','-','2000.00');
INSERT INTO `ysk_somebill` VALUES('420','196','6','佣金收入+','1555726365','+','20.00');
INSERT INTO `ysk_somebill` VALUES('421','196','5','充值2000.00确认-','1555726365','-','2000.00');
INSERT INTO `ysk_somebill` VALUES('422','196','6','充值100.00确认-','1555726494','-','100.00');
INSERT INTO `ysk_somebill` VALUES('423','196','6','佣金收入+','1555726494','+','1.00');
INSERT INTO `ysk_somebill` VALUES('424','196','5','充值100.00确认-','1555726494','-','100.00');
INSERT INTO `ysk_somebill` VALUES('425','196','6','充值100.00确认-','1555726853','-','100.00');
INSERT INTO `ysk_somebill` VALUES('426','196','6','佣金收入+','1555726853','+','1.00');
INSERT INTO `ysk_somebill` VALUES('427','196','5','充值100.00确认-','1555726853','-','100.00');
INSERT INTO `ysk_somebill` VALUES('428','196','6','充值1500.00确认-','1555731123','-','1500.00');
INSERT INTO `ysk_somebill` VALUES('429','196','6','佣金收入+','1555731123','+','15.00');
INSERT INTO `ysk_somebill` VALUES('430','196','5','充值1500.00确认-','1555731123','-','1500.00');
INSERT INTO `ysk_somebill` VALUES('431','196','6','充值1100.00确认-','1555732655','-','1100.00');
INSERT INTO `ysk_somebill` VALUES('432','196','6','佣金收入+','1555732655','+','11.00');
INSERT INTO `ysk_somebill` VALUES('433','196','5','充值1100.00确认-','1555732655','-','1100.00');
INSERT INTO `ysk_somebill` VALUES('434','196','6','充值80000.00确认-','1555732699','-','80000.00');
INSERT INTO `ysk_somebill` VALUES('435','196','6','佣金收入+','1555732699','+','800.00');
INSERT INTO `ysk_somebill` VALUES('436','196','5','充值80000.00确认-','1555732699','-','80000.00');
INSERT INTO `ysk_somebill` VALUES('437','196','6','充值2000.00确认-','1555735042','-','2000.00');
INSERT INTO `ysk_somebill` VALUES('438','196','6','佣金收入+','1555735042','+','20.00');
INSERT INTO `ysk_somebill` VALUES('439','196','5','充值2000.00确认-','1555735042','-','2000.00');
INSERT INTO `ysk_somebill` VALUES('440','196','6','充值20000.00确认-','1555735104','-','20000.00');
INSERT INTO `ysk_somebill` VALUES('441','196','6','佣金收入+','1555735104','+','200.00');
INSERT INTO `ysk_somebill` VALUES('442','196','5','充值20000.00确认-','1555735104','-','20000.00');
INSERT INTO `ysk_somebill` VALUES('443','196','6','提现退回+','1555741925','+','3000.00');
INSERT INTO `ysk_somebill` VALUES('445','197','6','佣金收入+','1555743242','+','0.10');
INSERT INTO `ysk_somebill` VALUES('446','197','5','充值10.00确认-','1555743242','-','10.00');
INSERT INTO `ysk_somebill` VALUES('447','197','6','充值20.00确认-','1555743644','-','20.00');
INSERT INTO `ysk_somebill` VALUES('448','197','6','佣金收入+','1555743644','+','0.20');
INSERT INTO `ysk_somebill` VALUES('449','197','5','充值20.00确认-','1555743644','-','20.00');
INSERT INTO `ysk_somebill` VALUES('450','197','6','充值10.00确认-','1555744957','-','10.00');
INSERT INTO `ysk_somebill` VALUES('451','197','6','佣金收入+','1555744957','+','0.10');
INSERT INTO `ysk_somebill` VALUES('452','197','5','充值10.00确认-','1555744957','-','10.00');
INSERT INTO `ysk_somebill` VALUES('453','197','6','充值500.00确认-','1555746313','-','500.00');
INSERT INTO `ysk_somebill` VALUES('454','197','6','佣金收入+','1555746313','+','5.00');
INSERT INTO `ysk_somebill` VALUES('455','197','5','充值500.00确认-','1555746313','-','500.00');
INSERT INTO `ysk_somebill` VALUES('456','197','6','充值10.00确认-','1555746362','-','10.00');
INSERT INTO `ysk_somebill` VALUES('457','197','6','佣金收入+','1555746362','+','0.10');
INSERT INTO `ysk_somebill` VALUES('458','197','5','充值10.00确认-','1555746362','-','10.00');
INSERT INTO `ysk_somebill` VALUES('459','197','6','充值400.00确认-','1555746491','-','400.00');
INSERT INTO `ysk_somebill` VALUES('460','197','6','佣金收入+','1555746491','+','4.00');
INSERT INTO `ysk_somebill` VALUES('461','197','5','充值400.00确认-','1555746491','-','400.00');
INSERT INTO `ysk_somebill` VALUES('462','197','6','充值100.00确认-','1555746524','-','100.00');
INSERT INTO `ysk_somebill` VALUES('463','197','6','佣金收入+','1555746524','+','1.00');
INSERT INTO `ysk_somebill` VALUES('464','197','5','充值100.00确认-','1555746524','-','100.00');
INSERT INTO `ysk_somebill` VALUES('465','197','6','充值10.00确认-','1555746618','-','10.00');
INSERT INTO `ysk_somebill` VALUES('466','197','6','佣金收入+','1555746618','+','0.10');
INSERT INTO `ysk_somebill` VALUES('467','197','5','充值10.00确认-','1555746618','-','10.00');
INSERT INTO `ysk_somebill` VALUES('468','197','6','充值1000.00确认-','1555746640','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('469','197','6','佣金收入+','1555746640','+','10.00');
INSERT INTO `ysk_somebill` VALUES('470','197','5','充值1000.00确认-','1555746640','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('471','197','6','充值1100.00确认-','1555746840','-','1100.00');
INSERT INTO `ysk_somebill` VALUES('472','197','6','佣金收入+','1555746840','+','11.00');
INSERT INTO `ysk_somebill` VALUES('473','197','5','充值1100.00确认-','1555746840','-','1100.00');
INSERT INTO `ysk_somebill` VALUES('474','197','6','充值1300.00确认-','1555746966','-','1300.00');
INSERT INTO `ysk_somebill` VALUES('475','197','6','佣金收入+','1555746966','+','13.00');
INSERT INTO `ysk_somebill` VALUES('476','197','5','充值1300.00确认-','1555746966','-','1300.00');
INSERT INTO `ysk_somebill` VALUES('477','197','6','充值10000.00确认-','1555748548','-','10000.00');
INSERT INTO `ysk_somebill` VALUES('478','197','6','佣金收入+','1555748548','+','100.00');
INSERT INTO `ysk_somebill` VALUES('479','197','5','充值10000.00确认-','1555748548','-','10000.00');
INSERT INTO `ysk_somebill` VALUES('480','196','6','提现退回+','1555749393','+','3000.00');
INSERT INTO `ysk_somebill` VALUES('481','196','6','充值1000.00确认-','1555753800','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('482','196','6','佣金收入+','1555753800','+','10.00');
INSERT INTO `ysk_somebill` VALUES('483','196','5','充值1000.00确认-','1555753800','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('484','196','6','充值1200.00确认-','1555759751','-','1200.00');
INSERT INTO `ysk_somebill` VALUES('485','196','6','佣金收入+','1555759751','+','12.00');
INSERT INTO `ysk_somebill` VALUES('486','196','5','充值1200.00确认-','1555759751','-','1200.00');
INSERT INTO `ysk_somebill` VALUES('487','196','6','充值18000.00确认-','1555764013','-','18000.00');
INSERT INTO `ysk_somebill` VALUES('488','196','6','佣金收入+','1555764013','+','180.00');
INSERT INTO `ysk_somebill` VALUES('489','196','5','充值18000.00确认-','1555764013','-','18000.00');
INSERT INTO `ysk_somebill` VALUES('490','196','6','充值2000.00确认-','1555764918','-','2000.00');
INSERT INTO `ysk_somebill` VALUES('491','196','6','佣金收入+','1555764918','+','20.00');
INSERT INTO `ysk_somebill` VALUES('492','196','5','充值2000.00确认-','1555764918','-','2000.00');
INSERT INTO `ysk_somebill` VALUES('493','196','6','充值2000.00确认-','1555766306','-','2000.00');
INSERT INTO `ysk_somebill` VALUES('494','196','6','佣金收入+','1555766306','+','24.00');
INSERT INTO `ysk_somebill` VALUES('495','196','5','充值2000.00确认-','1555766306','-','2000.00');
INSERT INTO `ysk_somebill` VALUES('496','196','6','充值9000.00确认-','1555768335','-','9000.00');
INSERT INTO `ysk_somebill` VALUES('497','196','6','佣金收入+','1555768335','+','108.00');
INSERT INTO `ysk_somebill` VALUES('498','196','5','充值9000.00确认-','1555768335','-','9000.00');
INSERT INTO `ysk_somebill` VALUES('499','196','5','支付宝抢单本金','1555771190','-','10.00');
INSERT INTO `ysk_somebill` VALUES('500','196','1','支付宝抢单佣金','1555771190','+','0.14');
INSERT INTO `ysk_somebill` VALUES('501','196','5','支付宝抢单本金','1555771246','-','10.00');
INSERT INTO `ysk_somebill` VALUES('502','196','1','支付宝抢单佣金','1555771246','+','0.14');
INSERT INTO `ysk_somebill` VALUES('503','196','6','充值150.00确认-','1555771304','-','150.00');
INSERT INTO `ysk_somebill` VALUES('504','196','6','佣金收入+','1555771304','+','1.80');
INSERT INTO `ysk_somebill` VALUES('505','196','5','充值150.00确认-','1555771304','-','150.00');
INSERT INTO `ysk_somebill` VALUES('506','196','6','充值108.00确认-','1555773614','-','108.00');
INSERT INTO `ysk_somebill` VALUES('507','196','6','佣金收入+','1555773614','+','1.30');
INSERT INTO `ysk_somebill` VALUES('508','196','5','充值108.00确认-','1555773614','-','108.00');
INSERT INTO `ysk_somebill` VALUES('509','196','6','充值1000.00确认-','1555780407','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('510','196','6','佣金收入+','1555780407','+','12.00');
INSERT INTO `ysk_somebill` VALUES('511','196','5','充值1000.00确认-','1555780407','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('512','196','6','充值1000.00确认-','1555780445','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('513','196','6','佣金收入+','1555780445','+','12.00');
INSERT INTO `ysk_somebill` VALUES('514','196','5','充值1000.00确认-','1555780445','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('515','196','6','充值1000.00确认-','1555780613','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('516','196','6','佣金收入+','1555780613','+','12.00');
INSERT INTO `ysk_somebill` VALUES('517','196','5','充值1000.00确认-','1555780613','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('518','196','6','充值100.00确认-','1555780716','-','100.00');
INSERT INTO `ysk_somebill` VALUES('519','196','6','佣金收入+','1555780716','+','1.20');
INSERT INTO `ysk_somebill` VALUES('520','196','5','充值100.00确认-','1555780716','-','100.00');
INSERT INTO `ysk_somebill` VALUES('521','196','6','充值100.00确认-','1555780982','-','100.00');
INSERT INTO `ysk_somebill` VALUES('522','196','6','佣金收入+','1555780982','+','1.20');
INSERT INTO `ysk_somebill` VALUES('523','196','5','充值100.00确认-','1555780982','-','100.00');
INSERT INTO `ysk_somebill` VALUES('524','196','6','充值100.00确认-','1555782244','-','100.00');
INSERT INTO `ysk_somebill` VALUES('525','196','6','佣金收入+','1555782244','+','1.20');
INSERT INTO `ysk_somebill` VALUES('526','196','5','充值100.00确认-','1555782244','-','100.00');
INSERT INTO `ysk_somebill` VALUES('527','196','6','充值120.00确认-','1555782489','-','120.00');
INSERT INTO `ysk_somebill` VALUES('528','196','6','佣金收入+','1555782489','+','1.20');
INSERT INTO `ysk_somebill` VALUES('529','196','5','充值120.00确认-','1555782489','-','120.00');
INSERT INTO `ysk_somebill` VALUES('530','196','6','充值159.00确认-','1555784145','-','159.00');
INSERT INTO `ysk_somebill` VALUES('531','196','6','佣金收入+','1555784145','+','1.91');
INSERT INTO `ysk_somebill` VALUES('532','196','5','充值159.00确认-','1555784145','-','159.00');
INSERT INTO `ysk_somebill` VALUES('533','196','6','充值88.00确认-','1555784704','-','88.00');
INSERT INTO `ysk_somebill` VALUES('534','196','6','佣金收入+','1555784704','+','1.06');
INSERT INTO `ysk_somebill` VALUES('535','196','5','充值88.00确认-','1555784704','-','88.00');
INSERT INTO `ysk_somebill` VALUES('536','196','6','充值100.00确认-','1555785260','-','100.00');
INSERT INTO `ysk_somebill` VALUES('537','196','6','佣金收入+','1555785260','+','1.20');
INSERT INTO `ysk_somebill` VALUES('538','196','5','充值100.00确认-','1555785260','-','100.00');
INSERT INTO `ysk_somebill` VALUES('539','196','6','充值88.00确认-','1555785283','-','88.00');
INSERT INTO `ysk_somebill` VALUES('540','196','6','佣金收入+','1555785283','+','0.88');
INSERT INTO `ysk_somebill` VALUES('541','196','5','充值88.00确认-','1555785283','-','88.00');
INSERT INTO `ysk_somebill` VALUES('542','196','6','充值150.00确认-','1555807038','-','150.00');
INSERT INTO `ysk_somebill` VALUES('543','196','6','佣金收入+','1555807038','+','1.80');
INSERT INTO `ysk_somebill` VALUES('544','196','5','充值150.00确认-','1555807038','-','150.00');
INSERT INTO `ysk_somebill` VALUES('545','196','6','充值120.00确认-','1555823211','-','120.00');
INSERT INTO `ysk_somebill` VALUES('546','196','6','佣金收入+','1555823211','+','1.44');
INSERT INTO `ysk_somebill` VALUES('547','196','5','充值120.00确认-','1555823211','-','120.00');
INSERT INTO `ysk_somebill` VALUES('548','196','6','充值100.00确认-','1555831201','-','100.00');
INSERT INTO `ysk_somebill` VALUES('549','196','6','佣金收入+','1555831201','+','1.20');
INSERT INTO `ysk_somebill` VALUES('550','196','5','充值100.00确认-','1555831201','-','100.00');
INSERT INTO `ysk_somebill` VALUES('551','196','6','充值125.00确认-','1555831266','-','125.00');
INSERT INTO `ysk_somebill` VALUES('552','196','6','佣金收入+','1555831266','+','1.50');
INSERT INTO `ysk_somebill` VALUES('553','196','5','充值125.00确认-','1555831266','-','125.00');
INSERT INTO `ysk_somebill` VALUES('554','197','6','充值10.00确认-','1555913884','-','10.00');
INSERT INTO `ysk_somebill` VALUES('555','197','6','佣金收入+','1555913884','+','0.10');
INSERT INTO `ysk_somebill` VALUES('556','197','5','充值10.00确认-','1555913884','-','10.00');
INSERT INTO `ysk_somebill` VALUES('557','197','6','充值10.00确认-','1555914097','-','10.00');
INSERT INTO `ysk_somebill` VALUES('558','197','6','佣金收入+','1555914097','+','0.10');
INSERT INTO `ysk_somebill` VALUES('559','197','5','充值10.00确认-','1555914097','-','10.00');
INSERT INTO `ysk_somebill` VALUES('560','197','6','充值10.00确认-','1555915024','-','10.00');
INSERT INTO `ysk_somebill` VALUES('561','197','6','佣金收入+','1555915024','+','0.12');
INSERT INTO `ysk_somebill` VALUES('562','197','5','充值10.00确认-','1555915024','-','10.00');
INSERT INTO `ysk_somebill` VALUES('563','197','6','充值666.00确认-','1555916905','-','666.00');
INSERT INTO `ysk_somebill` VALUES('564','197','6','佣金收入+','1555916905','+','7.99');
INSERT INTO `ysk_somebill` VALUES('565','197','5','充值666.00确认-','1555916905','-','666.00');
INSERT INTO `ysk_somebill` VALUES('566','197','6','充值169.00确认-','1555916930','-','169.00');
INSERT INTO `ysk_somebill` VALUES('567','197','6','佣金收入+','1555916930','+','2.03');
INSERT INTO `ysk_somebill` VALUES('568','197','5','充值169.00确认-','1555916930','-','169.00');
INSERT INTO `ysk_somebill` VALUES('569','197','6','充值10.00确认-','1555916942','-','10.00');
INSERT INTO `ysk_somebill` VALUES('570','197','6','佣金收入+','1555916942','+','0.12');
INSERT INTO `ysk_somebill` VALUES('571','197','5','充值10.00确认-','1555916942','-','10.00');
INSERT INTO `ysk_somebill` VALUES('572','197','6','充值10.00确认-','1555916966','-','10.00');
INSERT INTO `ysk_somebill` VALUES('573','197','6','佣金收入+','1555916966','+','0.10');
INSERT INTO `ysk_somebill` VALUES('574','197','5','充值10.00确认-','1555916966','-','10.00');
INSERT INTO `ysk_somebill` VALUES('575','197','6','充值998.00确认-','1555917583','-','998.00');
INSERT INTO `ysk_somebill` VALUES('576','197','6','佣金收入+','1555917583','+','11.98');
INSERT INTO `ysk_somebill` VALUES('577','197','5','充值998.00确认-','1555917583','-','998.00');
INSERT INTO `ysk_somebill` VALUES('578','198','6','充值120.00确认-','1555918717','-','120.00');
INSERT INTO `ysk_somebill` VALUES('579','198','6','佣金收入+','1555918717','+','1.20');
INSERT INTO `ysk_somebill` VALUES('580','198','5','充值120.00确认-','1555918717','-','120.00');
INSERT INTO `ysk_somebill` VALUES('581','198','6','充值188.00确认-','1555918860','-','188.00');
INSERT INTO `ysk_somebill` VALUES('582','198','6','佣金收入+','1555918860','+','1.88');
INSERT INTO `ysk_somebill` VALUES('583','198','5','充值188.00确认-','1555918860','-','188.00');
INSERT INTO `ysk_somebill` VALUES('584','198','6','充值153.00确认-','1555918873','-','153.00');
INSERT INTO `ysk_somebill` VALUES('585','198','6','佣金收入+','1555918873','+','1.53');
INSERT INTO `ysk_somebill` VALUES('586','198','5','充值153.00确认-','1555918873','-','153.00');
INSERT INTO `ysk_somebill` VALUES('587','198','6','充值188.00确认-','1555919022','-','188.00');
INSERT INTO `ysk_somebill` VALUES('588','198','6','佣金收入+','1555919022','+','1.88');
INSERT INTO `ysk_somebill` VALUES('589','198','5','充值188.00确认-','1555919022','-','188.00');
INSERT INTO `ysk_somebill` VALUES('590','198','6','充值199.00确认-','1555919038','-','199.00');
INSERT INTO `ysk_somebill` VALUES('591','198','6','佣金收入+','1555919038','+','1.99');
INSERT INTO `ysk_somebill` VALUES('592','198','5','充值199.00确认-','1555919038','-','199.00');
INSERT INTO `ysk_somebill` VALUES('593','198','6','充值233.00确认-','1555919113','-','233.00');
INSERT INTO `ysk_somebill` VALUES('594','198','6','佣金收入+','1555919113','+','2.33');
INSERT INTO `ysk_somebill` VALUES('595','198','5','充值233.00确认-','1555919113','-','233.00');
INSERT INTO `ysk_somebill` VALUES('596','198','6','充值244.00确认-','1555919195','-','244.00');
INSERT INTO `ysk_somebill` VALUES('597','198','6','佣金收入+','1555919195','+','2.93');
INSERT INTO `ysk_somebill` VALUES('598','198','5','充值244.00确认-','1555919195','-','244.00');
INSERT INTO `ysk_somebill` VALUES('599','198','6','充值145.00确认-','1555919279','-','145.00');
INSERT INTO `ysk_somebill` VALUES('600','198','6','佣金收入+','1555919279','+','1.74');
INSERT INTO `ysk_somebill` VALUES('601','198','5','充值145.00确认-','1555919279','-','145.00');
INSERT INTO `ysk_somebill` VALUES('602','198','6','提现退回+','1555919737','+','2000.00');
INSERT INTO `ysk_somebill` VALUES('603','198','6','充值250.00确认-','1555925798','-','250.00');
INSERT INTO `ysk_somebill` VALUES('604','198','6','佣金收入+','1555925798','+','2.50');
INSERT INTO `ysk_somebill` VALUES('605','198','5','充值250.00确认-','1555925798','-','250.00');
INSERT INTO `ysk_somebill` VALUES('606','198','6','充值230.00确认-','1555925861','-','230.00');
INSERT INTO `ysk_somebill` VALUES('607','198','6','佣金收入+','1555925861','+','2.30');
INSERT INTO `ysk_somebill` VALUES('608','198','5','充值230.00确认-','1555925861','-','230.00');
INSERT INTO `ysk_somebill` VALUES('609','198','6','充值230.00确认-','1555926076','-','230.00');
INSERT INTO `ysk_somebill` VALUES('610','198','6','佣金收入+','1555926076','+','2.30');
INSERT INTO `ysk_somebill` VALUES('611','198','5','充值230.00确认-','1555926076','-','230.00');
INSERT INTO `ysk_somebill` VALUES('612','198','6','充值1000.00确认-','1555926252','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('613','198','6','佣金收入+','1555926252','+','10.00');
INSERT INTO `ysk_somebill` VALUES('614','198','5','充值1000.00确认-','1555926252','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('615','198','6','充值1000.00确认-','1555926405','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('616','198','6','佣金收入+','1555926405','+','12.00');
INSERT INTO `ysk_somebill` VALUES('617','198','5','充值1000.00确认-','1555926405','-','1000.00');
INSERT INTO `ysk_somebill` VALUES('618','198','6','充值999.00确认-','1555926443','-','999.00');
INSERT INTO `ysk_somebill` VALUES('619','198','6','佣金收入+','1555926443','+','9.99');
INSERT INTO `ysk_somebill` VALUES('620','198','5','充值999.00确认-','1555926443','-','999.00');
INSERT INTO `ysk_somebill` VALUES('621','198','6','充值898.00确认-','1555926501','-','898.00');
INSERT INTO `ysk_somebill` VALUES('622','198','6','佣金收入+','1555926501','+','8.98');
INSERT INTO `ysk_somebill` VALUES('623','198','5','充值898.00确认-','1555926501','-','898.00');
INSERT INTO `ysk_somebill` VALUES('624','198','6','充值132.00确认-','1555926738','-','132.00');
INSERT INTO `ysk_somebill` VALUES('625','198','6','佣金收入+','1555926738','+','1.32');
INSERT INTO `ysk_somebill` VALUES('626','198','5','充值132.00确认-','1555926738','-','132.00');
INSERT INTO `ysk_somebill` VALUES('627','198','6','充值100.00确认-','1555927188','-','100.00');
INSERT INTO `ysk_somebill` VALUES('628','198','6','佣金收入+','1555927188','+','1.00');
INSERT INTO `ysk_somebill` VALUES('629','198','5','充值100.00确认-','1555927188','-','100.00');
INSERT INTO `ysk_somebill` VALUES('630','198','6','充值112.00确认-','1555927203','-','112.00');
INSERT INTO `ysk_somebill` VALUES('631','198','6','佣金收入+','1555927203','+','1.34');
INSERT INTO `ysk_somebill` VALUES('632','198','5','充值112.00确认-','1555927203','-','112.00');
INSERT INTO `ysk_somebill` VALUES('633','198','6','充值258.00确认-','1555928372','-','258.00');
INSERT INTO `ysk_somebill` VALUES('634','198','6','佣金收入+','1555928372','+','2.58');
INSERT INTO `ysk_somebill` VALUES('635','198','5','充值258.00确认-','1555928372','-','258.00');
INSERT INTO `ysk_somebill` VALUES('636','198','6','充值222.00确认-','1555928421','-','222.00');
INSERT INTO `ysk_somebill` VALUES('637','198','6','佣金收入+','1555928421','+','2.22');
INSERT INTO `ysk_somebill` VALUES('638','198','5','充值222.00确认-','1555928421','-','222.00');
INSERT INTO `ysk_somebill` VALUES('639','198','6','充值123.00确认-','1555928449','-','123.00');
INSERT INTO `ysk_somebill` VALUES('640','198','6','佣金收入+','1555928449','+','1.48');
INSERT INTO `ysk_somebill` VALUES('641','198','5','充值123.00确认-','1555928449','-','123.00');
INSERT INTO `ysk_somebill` VALUES('642','198','6','充值124.00确认-','1555928461','-','124.00');
INSERT INTO `ysk_somebill` VALUES('643','198','6','佣金收入+','1555928461','+','1.24');
INSERT INTO `ysk_somebill` VALUES('644','198','5','充值124.00确认-','1555928461','-','124.00');
INSERT INTO `ysk_somebill` VALUES('645','199','6','充值200.00确认-','1555942158','-','200.00');
INSERT INTO `ysk_somebill` VALUES('646','199','6','佣金收入+','1555942158','+','2.00');
INSERT INTO `ysk_somebill` VALUES('647','199','5','充值200.00确认-','1555942158','-','200.00');
INSERT INTO `ysk_somebill` VALUES('648','199','6','充值360.00确认-','1555942171','-','360.00');
INSERT INTO `ysk_somebill` VALUES('649','199','6','佣金收入+','1555942171','+','3.60');
INSERT INTO `ysk_somebill` VALUES('650','199','5','充值360.00确认-','1555942171','-','360.00');
INSERT INTO `ysk_somebill` VALUES('651','199','6','充值500.00确认-','1555942198','-','500.00');
INSERT INTO `ysk_somebill` VALUES('652','199','6','佣金收入+','1555942198','+','5.00');
INSERT INTO `ysk_somebill` VALUES('653','199','5','充值500.00确认-','1555942198','-','500.00');
INSERT INTO `ysk_somebill` VALUES('654','199','6','充值300.00确认-','1555942216','-','300.00');
INSERT INTO `ysk_somebill` VALUES('655','199','6','佣金收入+','1555942216','+','3.00');
INSERT INTO `ysk_somebill` VALUES('656','199','5','充值300.00确认-','1555942216','-','300.00');
INSERT INTO `ysk_somebill` VALUES('657','199','6','充值200.00确认-','1555942228','-','200.00');
INSERT INTO `ysk_somebill` VALUES('658','199','6','佣金收入+','1555942228','+','2.00');
INSERT INTO `ysk_somebill` VALUES('659','199','5','充值200.00确认-','1555942228','-','200.00');
INSERT INTO `ysk_somebill` VALUES('660','199','6','充值50.00确认-','1555942302','-','50.00');
INSERT INTO `ysk_somebill` VALUES('661','199','6','佣金收入+','1555942302','+','0.50');
INSERT INTO `ysk_somebill` VALUES('662','199','5','充值50.00确认-','1555942302','-','50.00');
INSERT INTO `ysk_somebill` VALUES('663','199','6','充值241.00确认-','1555942735','-','241.00');
INSERT INTO `ysk_somebill` VALUES('664','199','6','佣金收入+','1555942735','+','2.41');
INSERT INTO `ysk_somebill` VALUES('665','199','5','充值241.00确认-','1555942735','-','241.00');
INSERT INTO `ysk_somebill` VALUES('666','200','6','充值130.00确认-','1555943659','-','130.00');
INSERT INTO `ysk_somebill` VALUES('667','200','6','佣金收入+','1555943659','+','1.30');
INSERT INTO `ysk_somebill` VALUES('668','200','5','充值130.00确认-','1555943659','-','130.00');
INSERT INTO `ysk_somebill` VALUES('669','199','6','充值258.00确认-','1555948818','-','258.00');
INSERT INTO `ysk_somebill` VALUES('670','199','6','佣金收入+','1555948818','+','2.58');
INSERT INTO `ysk_somebill` VALUES('671','199','5','充值258.00确认-','1555948818','-','258.00');
INSERT INTO `ysk_somebill` VALUES('672','201','6','充值150.00确认-','1555953763','-','150.00');
INSERT INTO `ysk_somebill` VALUES('673','201','6','佣金收入+','1555953763','+','1.50');
INSERT INTO `ysk_somebill` VALUES('674','201','5','充值150.00确认-','1555953763','-','150.00');
INSERT INTO `ysk_somebill` VALUES('675','201','6','充值369.00确认-','1555954051','-','369.00');
INSERT INTO `ysk_somebill` VALUES('676','201','6','佣金收入+','1555954051','+','3.69');
INSERT INTO `ysk_somebill` VALUES('677','201','5','充值369.00确认-','1555954051','-','369.00');
INSERT INTO `ysk_somebill` VALUES('678','202','6','充值20000.00确认-','1555954788','-','20000.00');
INSERT INTO `ysk_somebill` VALUES('679','202','6','佣金收入+','1555954788','+','200.00');
INSERT INTO `ysk_somebill` VALUES('680','202','5','充值20000.00确认-','1555954788','-','20000.00');
INSERT INTO `ysk_somebill` VALUES('681','202','6','充值78000.00确认-','1555954896','-','78000.00');
INSERT INTO `ysk_somebill` VALUES('682','202','6','佣金收入+','1555954896','+','780.00');
INSERT INTO `ysk_somebill` VALUES('683','202','5','充值78000.00确认-','1555954896','-','78000.00');
INSERT INTO `ysk_somebill` VALUES('684','202','6','充值50000.00确认-','1555955020','-','50000.00');
INSERT INTO `ysk_somebill` VALUES('685','202','6','佣金收入+','1555955020','+','500.00');
INSERT INTO `ysk_somebill` VALUES('686','202','5','充值50000.00确认-','1555955020','-','50000.00');
INSERT INTO `ysk_somebill` VALUES('687','201','6','充值50000.00确认-','1555956121','-','50000.00');
INSERT INTO `ysk_somebill` VALUES('688','201','6','佣金收入+','1555956121','+','500.00');
INSERT INTO `ysk_somebill` VALUES('689','201','5','充值50000.00确认-','1555956121','-','50000.00');
INSERT INTO `ysk_somebill` VALUES('690','199','6','充值258.00确认-','1555986236','-','258.00');
INSERT INTO `ysk_somebill` VALUES('691','199','6','佣金收入+','1555986236','+','2.58');
INSERT INTO `ysk_somebill` VALUES('692','199','5','充值258.00确认-','1555986236','-','258.00');
INSERT INTO `ysk_somebill` VALUES('693','199','6','充值88.00确认-','1556019339','-','88.00');
INSERT INTO `ysk_somebill` VALUES('694','199','6','佣金收入+','1556019339','+','0.88');
INSERT INTO `ysk_somebill` VALUES('695','199','5','充值88.00确认-','1556019339','-','88.00');
INSERT INTO `ysk_somebill` VALUES('696','199','6','充值1055.00确认-','1556019399','-','1055.00');
INSERT INTO `ysk_somebill` VALUES('697','199','6','佣金收入+','1556019399','+','10.55');
INSERT INTO `ysk_somebill` VALUES('698','199','5','充值1055.00确认-','1556019399','-','1055.00');
INSERT INTO `ysk_somebill` VALUES('699','199','6','充值99.00确认-','1556031327','-','99.00');
INSERT INTO `ysk_somebill` VALUES('700','199','6','佣金收入+','1556031327','+','0.99');
INSERT INTO `ysk_somebill` VALUES('701','199','5','充值99.00确认-','1556031327','-','99.00');
INSERT INTO `ysk_somebill` VALUES('702','199','6','充值258.00确认-','1556031463','-','258.00');
INSERT INTO `ysk_somebill` VALUES('703','199','6','佣金收入+','1556031463','+','2.58');
INSERT INTO `ysk_somebill` VALUES('704','199','5','充值258.00确认-','1556031463','-','258.00');
INSERT INTO `ysk_somebill` VALUES('705','199','6','充值258.00确认-','1556032496','-','258.00');
INSERT INTO `ysk_somebill` VALUES('706','199','6','佣金收入+','1556032496','+','2.58');
INSERT INTO `ysk_somebill` VALUES('707','199','5','充值258.00确认-','1556032496','-','258.00');
--
-- 表的结构ysk_store
--
DROP TABLE IF EXISTS `ysk_store`;
CREATE TABLE `ysk_store` (
  `uid` int(11) unsigned NOT NULL COMMENT '用户id',
  `cangku_num` decimal(13,5) NOT NULL DEFAULT '0.00000' COMMENT '钱包余额',
  `fengmi_num` decimal(13,5) NOT NULL DEFAULT '0.00000' COMMENT '积分',
  `plant_num` decimal(13,4) NOT NULL DEFAULT '0.0000' COMMENT '播种总数',
  `huafei_total` decimal(13,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '施肥累计',
  `vip_grade` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

--
-- 转存表中的数据 ysk_store

--
-- 表的结构ysk_system
--
DROP TABLE IF EXISTS `ysk_system`;
CREATE TABLE `ysk_system` (
  `id` int(2) NOT NULL AUTO_INCREMENT COMMENT '记录ID',
  `qd_cf` int(11) NOT NULL COMMENT '抢单余额比列',
  `qd_nd` varchar(225) NOT NULL COMMENT '抢单难度，数组(0.1,0.2,0.3)',
  `qd_wxyj` float(10,3) NOT NULL COMMENT '微信抢单佣金30%填0.3',
  `qd_zfbyj` float(10,3) NOT NULL COMMENT '支付宝抢单佣金30%填0.3',
  `qd_bkyj` float(10,3) NOT NULL COMMENT '银行卡抢单佣金30%填0.3',
  `qd_ndtime` varchar(225) NOT NULL COMMENT '增加难度时间点',
  `qd_yjjc` varchar(12) NOT NULL COMMENT '佣金加成',
  `qd_minmoney` float NOT NULL COMMENT '抢单最低额度',
  `min_recharge` float(10,3) NOT NULL COMMENT '最低充值额度',
  `mix_withdraw` float(10,3) NOT NULL COMMENT '最小提现额度',
  `max_withdraw` float(10,3) NOT NULL COMMENT '最大提现额度',
  `tx_yeb` float NOT NULL COMMENT '提现要求：收款与余额比例 ',
  `team_oneyj` float(10,3) NOT NULL COMMENT '1代佣金比例30%写0.3',
  `team_twoyj` float(10,3) NOT NULL COMMENT '2代佣金比例30%写0.3',
  `team_threeyj` float(10,2) NOT NULL COMMENT '3代佣金比例30%写0.3',
  `cz_yh` varchar(255) NOT NULL,
  `cz_xm` varchar(255) NOT NULL,
  `cz_kh` varchar(255) NOT NULL,
  `wxb` varchar(255) DEFAULT NULL,
  `zfbb` varchar(255) DEFAULT NULL,
  `ylb` varchar(255) DEFAULT NULL,
  `ed` decimal(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='游戏参数设置表';

--
-- 转存表中的数据 ysk_system

INSERT INTO `ysk_system` VALUES('1','100','2','0.014','0.014','0.014','2','0.001','2000','5000.000','100.000','50000.000','0','0.100','0.100','0.10','建设银行','李春','622341567899','1.2','1','0','2000.00');
--
-- 表的结构ysk_upload
--
DROP TABLE IF EXISTS `ysk_upload`;
CREATE TABLE `ysk_upload` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'UID',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '文件名',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '文件路径',
  `url` varchar(255) DEFAULT NULL COMMENT '文件链接',
  `ext` char(4) NOT NULL DEFAULT '' COMMENT '文件类型',
  `size` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `md5` char(32) DEFAULT NULL COMMENT '文件md5',
  `sha1` char(40) DEFAULT NULL COMMENT '文件sha1编码',
  `location` varchar(15) NOT NULL DEFAULT '' COMMENT '文件存储位置',
  `download` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '下载次数',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上传时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '修改时间',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='文件上传表';

--
-- 转存表中的数据 ysk_upload

--
-- 表的结构ysk_user
--
DROP TABLE IF EXISTS `ysk_user`;
CREATE TABLE `ysk_user` (
  `userid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL COMMENT '上级ID',
  `gid` int(11) NOT NULL DEFAULT '0' COMMENT '上上级ID',
  `ggid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上上上级ID',
  `account` char(20) NOT NULL DEFAULT '0' COMMENT '用户账号',
  `mobile` char(20) NOT NULL COMMENT '用户手机号',
  `u_yqm` varchar(225) NOT NULL COMMENT '邀请码',
  `username` varchar(255) NOT NULL DEFAULT '',
  `login_pwd` varchar(225) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `login_salt` char(5) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `money` float(10,2) NOT NULL DEFAULT '0.00' COMMENT '用户余额',
  `reg_date` int(11) NOT NULL COMMENT '注册时间',
  `reg_ip` varchar(20) NOT NULL COMMENT '注册IP',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '用户锁定  1 不锁  0拉黑  -1 删除',
  `activate` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否激活 1-已激活 0-未激活',
  `session_id` varchar(225) DEFAULT NULL,
  `wx_no` varchar(225) DEFAULT NULL COMMENT '微信账号',
  `alipay` varchar(225) DEFAULT NULL COMMENT '支付宝',
  `truename` varchar(225) DEFAULT NULL COMMENT '真实姓名',
  `email` varchar(225) NOT NULL COMMENT '电子邮件',
  `userqq` varchar(32) NOT NULL COMMENT 'QQ',
  `usercard` varchar(32) NOT NULL COMMENT '身份证号码',
  `path` text,
  `use_grade` tinyint(1) NOT NULL DEFAULT '0' COMMENT '用户等级',
  `u_ztnum` int(11) NOT NULL COMMENT '直推人数',
  `rz_st` int(1) NOT NULL COMMENT '资料完善状态，1OK2no',
  `tx_status` int(11) NOT NULL DEFAULT '1' COMMENT '提现状态',
  `zsy` float(10,2) NOT NULL COMMENT '总收益',
  `agent` int(11) DEFAULT '0',
  `num` int(11) DEFAULT NULL,
  `zdopention` int(11) DEFAULT NULL,
  PRIMARY KEY (`userid`) USING BTREE,
  UNIQUE KEY `mobile` (`mobile`) USING BTREE,
  UNIQUE KEY `account` (`account`) USING BTREE,
  KEY `username` (`username`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=206 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 ysk_user

--
-- 表的结构ysk_userrob
--
DROP TABLE IF EXISTS `ysk_userrob`;
CREATE TABLE `ysk_userrob` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` int(11) NOT NULL COMMENT '会员ID',
  `class` int(2) NOT NULL COMMENT '支付类别',
  `price` float(10,2) NOT NULL COMMENT '金额',
  `yjjc` float(10,2) NOT NULL COMMENT '佣金加成',
  `umoney` float(10,2) NOT NULL COMMENT '会员余额',
  `uaccount` varchar(225) NOT NULL COMMENT '会员账号',
  `uname` varchar(225) NOT NULL COMMENT '会员姓名',
  `ppid` int(11) NOT NULL COMMENT '匹配的ID号',
  `status` int(2) NOT NULL COMMENT '状态1等2匹配中3成功',
  `addtime` varchar(225) NOT NULL COMMENT '添加时间',
  `pipeitime` varchar(225) NOT NULL COMMENT '匹配成功时间',
  `finishtime` varchar(225) NOT NULL COMMENT '交易完成时间',
  `ordernum` varchar(225) NOT NULL COMMENT '订单号',
  `pay_sn` varchar(255) DEFAULT NULL COMMENT '充值编号',
  `pay_money` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=408 DEFAULT CHARSET=utf8 COMMENT='会员抢单表前台发起的';

--
-- 转存表中的数据 ysk_userrob

INSERT INTO `ysk_userrob` VALUES('282','196','1','10.00','0.00','19000.00','18248654865','','632','4','1555763306','1555763306','','N407180955683','E1555763210687','10.00');
INSERT INTO `ysk_userrob` VALUES('287','197','1','1.00','0.00','19000.00','13800138000','测试','650','4','1555768477','1555768477','','N429409440015','E1555764886828','1.00');
INSERT INTO `ysk_userrob` VALUES('288','197','1','5.00','0.00','18999.00','13800138000','测试','654','4','1555768961','1555768961','','N233748624526','E1555768947729','5.00');
INSERT INTO `ysk_userrob` VALUES('289','197','1','10.00','0.00','18994.00','13800138000','测试','658','4','1555769462','1555769462','','N652921334380','E1555769389816','10.00');
INSERT INTO `ysk_userrob` VALUES('290','197','1','10.00','0.00','18984.00','13800138000','测试','659','4','1555769466','1555769466','','N686739056234','E155576944812','10.00');
INSERT INTO `ysk_userrob` VALUES('291','197','2','10.00','0.00','18974.00','13800138000','测试','661','4','1555769587','1555769587','','N705647416826','E1555769567422','10.00');
INSERT INTO `ysk_userrob` VALUES('292','197','1','10.00','0.00','18964.00','13800138000','测试','660','4','1555769592','1555769592','','N777596737441','E1555769542616','10.00');
INSERT INTO `ysk_userrob` VALUES('293','196','1','10.00','0.00','2100.00','18248654865','','663','4','1555769673','1555769673','','N616512737731','E155576964692','10.00');
INSERT INTO `ysk_userrob` VALUES('294','196','1','10.00','0.00','2090.00','18248654865','','664','4','1555769700','1555769700','','N239213741989','E1555769682165','10.00');
INSERT INTO `ysk_userrob` VALUES('295','196','2','10.00','0.00','2080.00','18248654865','','669','4','1555770037','1555770037','1555771190','N428086352023','E1555770027266','10.00');
INSERT INTO `ysk_userrob` VALUES('296','196','2','10.00','0.00','2070.00','18248654865','','670','4','1555770099','1555770099','1555771246','N128105530468','E15557700682','10.00');
INSERT INTO `ysk_userrob` VALUES('297','196','2','10.00','0.00','2060.00','18248654865','','672','4','1555770273','1555770273','','N816386764576','E1555770187809','10.00');
INSERT INTO `ysk_userrob` VALUES('298','196','1','150.00','0.00','5000.00','18248654865','','673','4','1555770505','1555770505','1555771304','N148180813253','E1555770324367','150.00');
INSERT INTO `ysk_userrob` VALUES('299','196','1','120.00','0.00','4852.08','18248654865','张三','675','4','1555771361','1555771361','','N101946280519','E1555771311509','120.00');
INSERT INTO `ysk_userrob` VALUES('301','196','1','1000.00','0.00','4745.38','18248654865','张三','679','4','1555775663','1555775663','','N792436564550','E1555775157364','1000.00');
INSERT INTO `ysk_userrob` VALUES('309','196','2','155.00','0.00','3596.80','18248654865','张三','693','4','1555782633','1555782633','','N099213548889','E1555782530130','155.00');
INSERT INTO `ysk_userrob` VALUES('310','196','2','155.00','0.00','3441.80','18248654865','张三','694','4','1555782688','1555782688','','N665308549993','E1555782608370','155.00');
INSERT INTO `ysk_userrob` VALUES('313','196','2','88.00','0.00','3197.77','18248654865','张三','698','3','1555784737','1555784737','1555785283','N483999330829','E1555784710733','88.00');
INSERT INTO `ysk_userrob` VALUES('314','196','1','100.00','0.00','3109.77','18248654865','张三','699','3','1555785184','1555785184','1555785260','N001969471978','E1555785133728','100.00');
INSERT INTO `ysk_userrob` VALUES('315','196','1','150.00','0.00','3011.85','18248654865','张三','702','3','1555807026','1555807026','1555807038','N859947898018','E1555806974870','150.00');
INSERT INTO `ysk_userrob` VALUES('316','196','1','10.00','0.00','2863.65','18248654865','张三','708','4','1555807738','1555807738','','N743998356508','E1555807710132','10.00');
INSERT INTO `ysk_userrob` VALUES('317','196','1','120.00','0.00','2863.65','18248654865','张三','709','3','1555823020','1555823020','1555823211','N022688159133','E1555822144955','120.00');
INSERT INTO `ysk_userrob` VALUES('320','196','1','10.00','0.00','9777.70','18248654865','张三','713','4','1555831310','1555831310','','N798208567177','E1555831297226','10.00');
INSERT INTO `ysk_userrob` VALUES('321','197','2','10.00','0.00','18965.00','13800138000','测试','720','4','1555855576','1555855576','','N019191826494','E1555855561757','10.00');
INSERT INTO `ysk_userrob` VALUES('322','197','2','10.00','0.00','18955.00','13800138000','测试','721','4','1555855913','1555855913','','N507827285777','E1555855886538','10.00');
INSERT INTO `ysk_userrob` VALUES('323','197','2','10.00','0.00','18945.00','13800138000','测试','722','4','1555856342','1555856342','','N912082093803','E1555856320645','10.00');
INSERT INTO `ysk_userrob` VALUES('324','197','2','10.00','0.00','18935.00','13800138000','测试','723','4','1555856436','1555856436','','N338667507387','E1555856398669','10.00');
INSERT INTO `ysk_userrob` VALUES('325','197','2','10.00','0.00','18925.00','13800138000','测试','724','4','1555856484','1555856484','','N360990829811','E1555856467522','10.00');
INSERT INTO `ysk_userrob` VALUES('326','197','2','10.00','0.00','18925.00','13800138000','测试','725','4','1555856633','1555856633','','N244391787131','E1555856619655','10.00');
INSERT INTO `ysk_userrob` VALUES('327','197','2','10.00','0.00','18915.00','13800138000','测试','726','4','1555856729','1555856729','','N484761131171','E1555856720560','10.00');
INSERT INTO `ysk_userrob` VALUES('328','197','2','10.00','0.00','18905.00','13800138000','测试','727','4','1555856889','1555856889','','N560177943027','E155585688152','10.00');
INSERT INTO `ysk_userrob` VALUES('329','197','2','10.00','0.00','18905.00','13800138000','测试','728','4','1555857110','1555857110','','N743371418673','E1555857097296','10.00');
INSERT INTO `ysk_userrob` VALUES('330','197','2','10.00','0.00','18905.00','13800138000','测试','743','4','1555864492','1555864492','','N160253388566','E1555864274763','10.00');
INSERT INTO `ysk_userrob` VALUES('331','197','2','10.00','0.00','18895.00','13800138000','测试','745','4','1555864755','1555864755','','N816721634172','E1555864740835','10.00');
INSERT INTO `ysk_userrob` VALUES('332','197','2','30.00','0.00','18885.00','13800138000','测试','746','4','1555864796','1555864796','','N584413519547','E1555864776871','30.00');
INSERT INTO `ysk_userrob` VALUES('333','197','2','10.00','0.00','18865.00','13800138000','测试','758','3','1555913871','1555913871','1555913884','N151571072388','E15559138199481','10.00');
INSERT INTO `ysk_userrob` VALUES('334','197','2','10.00','0.00','18855.10','13800138000','测试','759','3','1555913917','1555913917','1555914097','N660626339494','E15559138868867','10.00');
INSERT INTO `ysk_userrob` VALUES('335','197','1','10.00','0.00','18845.20','13800138000','测试','760','4','1555914198','1555914198','','N297459913102','E15559141016589','10.00');
INSERT INTO `ysk_userrob` VALUES('336','197','2','10.00','0.00','18835.20','13800138000','测试','761','4','1555914829','1555914829','','N369924114165','E1555914803191','10.00');
INSERT INTO `ysk_userrob` VALUES('337','197','1','10.00','0.00','18825.20','13800138000','测试','762','4','1555914949','1555914949','','N846840651793','E15559149465012','10.00');
INSERT INTO `ysk_userrob` VALUES('338','197','1','10.00','0.00','18815.20','13800138000','测试','763','3','1555914993','1555914993','1555915024','N486954018738','E15559149792943','10.00');
INSERT INTO `ysk_userrob` VALUES('339','197','1','10.00','0.00','18805.32','13800138000','测试','764','4','1555915065','1555915065','','N085474761386','E15559150615086','10.00');
INSERT INTO `ysk_userrob` VALUES('340','197','2','10.00','0.00','18805.32','13800138000','测试','765','4','1555916320','1555916320','','N267086655168','E15559163059495','10.00');
INSERT INTO `ysk_userrob` VALUES('341','197','2','10.00','0.00','18795.32','13800138000','测试','767','3','1555916490','1555916490','1555916966','N565785748741','E15559164755339','10.00');
INSERT INTO `ysk_userrob` VALUES('342','197','1','10.00','0.00','18785.32','13800138000','测试','768','3','1555916545','1555916545','1555916942','N435363807173','E1555916532802','10.00');
INSERT INTO `ysk_userrob` VALUES('343','197','1','169.00','0.00','18775.32','13800138000','测试','769','3','1555916626','1555916626','1555916930','N214647057864','E1555916624451','169.00');
INSERT INTO `ysk_userrob` VALUES('344','197','1','666.00','0.00','18606.32','13800138000','测试','770','3','1555916848','1555916848','1555916905','N458143181998','E15559168436086','666.00');
INSERT INTO `ysk_userrob` VALUES('345','197','1','10.00','0.00','17950.56','13800138000','测试','771','4','1555917029','1555917029','','N009974797538','E15559170161260','10.00');
INSERT INTO `ysk_userrob` VALUES('346','197','2','10.00','0.00','17940.56','13800138000','测试','772','4','1555917194','1555917194','','N574936179203','E1555917172825','10.00');
INSERT INTO `ysk_userrob` VALUES('347','197','1','998.00','0.00','17940.56','13800138000','测试','773','3','1555917439','1555917439','1555917583','N249538584284','E15559171798750','998.00');
INSERT INTO `ysk_userrob` VALUES('348','197','1','369.00','0.00','16954.54','13800138000','测试','774','4','1555917631','1555917631','','N684540769799','E15559176211589','369.00');
INSERT INTO `ysk_userrob` VALUES('349','197','2','800.00','0.00','16585.54','13800138000','测试','775','4','1555917712','1555917712','','N179096064972','E15559177002803','800.00');
INSERT INTO `ysk_userrob` VALUES('350','198','2','120.00','0.00','10635.00','18248654865','张三','776','3','1555918699','1555918699','1555918717','N125407026822','E15559186782164','120.00');
INSERT INTO `ysk_userrob` VALUES('351','198','2','153.00','0.00','10516.20','18248654865','张三','777','3','1555918788','1555918788','1555918873','N198655909041','E15559187337698','153.00');
INSERT INTO `ysk_userrob` VALUES('352','198','2','188.00','0.00','10363.20','18248654865','张三','778','3','1555918833','1555918833','1555918860','N006752837291','E15559188071621','188.00');
INSERT INTO `ysk_userrob` VALUES('353','198','2','188.00','0.00','10178.61','18248654865','张三','779','3','1555918892','1555918892','1555919022','N700700929711','E15559188862805','188.00');
INSERT INTO `ysk_userrob` VALUES('354','198','2','199.00','0.00','9990.61','18248654865','张三','780','3','1555918965','1555918965','1555919038','N799356986758','E15559189504686','199.00');
INSERT INTO `ysk_userrob` VALUES('355','198','2','233.00','0.00','9795.48','18248654865','张三','781','3','1555919097','1555919097','1555919113','N392689418526','E15559190686551','233.00');
INSERT INTO `ysk_userrob` VALUES('356','198','1','244.00','0.00','9564.81','18248654865','张三','782','3','1555919175','1555919175','1555919195','N107485788875','E15559191658798','244.00');
INSERT INTO `ysk_userrob` VALUES('357','198','1','145.00','0.00','9323.74','18248654865','张三','783','3','1555919267','1555919267','1555919279','N380011970411','E15559192128540','145.00');
INSERT INTO `ysk_userrob` VALUES('358','198','2','250.00','0.00','9180.48','18248654865','张三','785','3','1555925783','1555925783','1555925798','N309146088890','E15559257778617','250.00');
INSERT INTO `ysk_userrob` VALUES('359','198','2','230.00','0.00','8932.98','18248654865','张三','786','3','1555925843','1555925843','1555925861','N123641759062','E15559258202747','230.00');
INSERT INTO `ysk_userrob` VALUES('360','198','2','230.00','0.00','8705.28','18248654865','张三','787','3','1555925898','1555925898','1555926076','N737224330528','E15559258959496','230.00');
INSERT INTO `ysk_userrob` VALUES('361','198','2','1000.00','0.00','8477.58','18248654865','张三','790','3','1555926235','1555926235','1555926252','N219641542715','E15559262042853','1000.00');
INSERT INTO `ysk_userrob` VALUES('362','198','1','1000.00','0.00','7487.58','18248654865','张三','791','3','1555926391','1555926391','1555926405','N341916143304','E15559263455735','1000.00');
INSERT INTO `ysk_userrob` VALUES('363','198','2','999.00','0.00','6499.58','18248654865','张三','792','3','1555926432','1555926432','1555926443','N617103240271','E15559264237513','999.00');
INSERT INTO `ysk_userrob` VALUES('364','198','2','898.00','0.00','5510.57','18248654865','张三','793','3','1555926482','1555926482','1555926501','N539853186688','E15559264731081','898.00');
INSERT INTO `ysk_userrob` VALUES('365','198','2','132.00','0.00','4621.55','18248654865','张三','795','3','1555926672','1555926672','1555926738','N808886443710','E15559266391608','132.00');
INSERT INTO `ysk_userrob` VALUES('366','198','2','100.00','0.00','4490.87','18248654865','张三','797','3','1555926945','1555926945','1555927188','N162581196198','E15559269396917','100.00');
INSERT INTO `ysk_userrob` VALUES('367','198','1','112.00','0.00','4390.87','18248654865','张三','798','3','1555927162','1555927162','1555927203','N241165459786','E15559271241414','112.00');
INSERT INTO `ysk_userrob` VALUES('368','198','2','100.00','0.00','4281.21','18248654865','张三','800','4','1555927304','1555927304','','N428151477168','E15559273004815','100.00');
INSERT INTO `ysk_userrob` VALUES('369','198','2','50.00','0.00','4181.21','18248654865','张三','801','4','1555927383','1555927383','','N379458961352','E15559273797124','50.00');
INSERT INTO `ysk_userrob` VALUES('370','198','2','124.00','0.00','4131.21','18248654865','张三','802','3','1555927605','1555927605','1555928461','N970193487950','E15559275869616','124.00');
INSERT INTO `ysk_userrob` VALUES('371','198','1','123.00','0.00','4007.21','18248654865','张三','803','3','1555927760','1555927760','1555928449','N153375907983','E15559277471828','123.00');
INSERT INTO `ysk_userrob` VALUES('372','198','2','258.00','0.00','3984.21','18248654865','张三','806','3','1555928317','1555928317','1555928372','N307388411274','E15559282818469','258.00');
INSERT INTO `ysk_userrob` VALUES('373','198','2','222.00','0.00','3778.79','18248654865','张三','807','3','1555928411','1555928411','1555928421','N110216315617','E15559282877634','222.00');
INSERT INTO `ysk_userrob` VALUES('374','199','2','200.00','0.00','10000.00','18248654865','左刚','813','3','1555941420','1555941420','1555942228','N320910747949','E15559414189577','200.00');
INSERT INTO `ysk_userrob` VALUES('375','199','2','200.00','0.00','9800.00','18248654865','左刚','814','3','1555941488','1555941488','1555942158','N646405599392','E15559414563488','200.00');
INSERT INTO `ysk_userrob` VALUES('376','199','2','300.00','0.00','9600.00','18248654865','左刚','815','3','1555941638','1555941638','1555942216','N235140297361','E15559416349013','300.00');
INSERT INTO `ysk_userrob` VALUES('377','199','2','500.00','0.00','9300.00','18248654865','左刚','816','3','1555941848','1555941848','1555942198','N534008873372','E15559418457674','500.00');
INSERT INTO `ysk_userrob` VALUES('378','199','2','360.00','0.00','8800.00','18248654865','左刚','817','3','1555942003','1555942003','1555942171','N069982404540','E15559420001678','360.00');
INSERT INTO `ysk_userrob` VALUES('379','199','2','50.00','0.00','8455.60','18248654865','左刚','818','3','1555942238','1555942238','1555942302','N982223621679','E15559422043148','50.00');
INSERT INTO `ysk_userrob` VALUES('380','199','2','134.00','0.00','8406.10','18248654865','左刚','819','4','1555942467','1555942467','','N319066756757','E1555942454687','134.00');
INSERT INTO `ysk_userrob` VALUES('381','199','2','241.00','0.00','8272.10','18248654865','左刚','820','3','1555942570','1555942570','1555942735','N703420133596','E1555942547467','241.00');
INSERT INTO `ysk_userrob` VALUES('382','199','2','205.00','0.00','8031.10','18248654865','左刚','821','4','1555942611','1555942611','','N469335973749','E15559426074132','205.00');
INSERT INTO `ysk_userrob` VALUES('383','199','2','245.00','0.00','7826.10','18248654865','左刚','823','4','1555942716','1555942716','','N286904213805','E15559427139260','245.00');
INSERT INTO `ysk_userrob` VALUES('384','200','2','130.00','0.00','10000.00','13222222222','','827','3','1555943641','1555943641','1555943659','N948624754588','E15559436333018','130.00');
INSERT INTO `ysk_userrob` VALUES('385','199','2','260.00','0.00','7828.51','18248654865','左刚','828','4','1555946175','1555946175','','N612894495620','E15559461709357','260.00');
INSERT INTO `ysk_userrob` VALUES('386','199','2','222.00','0.00','7568.51','18248654865','左刚','829','4','1555946424','1555946424','','N069321302702','E15559464151514','222.00');
INSERT INTO `ysk_userrob` VALUES('387','199','2','444.00','0.00','7346.51','18248654865','左刚','830','4','1555946456','1555946456','','N334373664841','E1555946452853','444.00');
INSERT INTO `ysk_userrob` VALUES('388','199','2','234.00','0.00','6902.51','18248654865','左刚','831','4','1555946486','1555946486','','N427267812649','E15559464756315','234.00');
INSERT INTO `ysk_userrob` VALUES('389','199','2','234.00','0.00','6668.51','18248654865','左刚','832','4','1555946520','1555946520','','N625433381180','E15559465062502','234.00');
INSERT INTO `ysk_userrob` VALUES('390','199','2','212.00','0.00','6434.51','18248654865','左刚','833','4','1555946552','1555946552','','N754025113330','E15559465489442','212.00');
INSERT INTO `ysk_userrob` VALUES('391','199','2','10.00','0.00','6222.51','18248654865','左刚','834','4','1555947529','1555947529','','N232053611055','E15559475261326','10.00');
INSERT INTO `ysk_userrob` VALUES('392','199','2','258.00','0.00','6222.51','18248654865','左刚','835','3','1555948761','1555948761','1555948818','N995814687661','E15559487435679','258.00');
INSERT INTO `ysk_userrob` VALUES('393','199','2','258.00','0.00','5967.09','18248654865','左刚','836','4','1555948883','1555948883','','N574721452204','E15559488669256','258.00');
INSERT INTO `ysk_userrob` VALUES('394','199','2','25.00','0.00','5967.09','18248654865','左刚','837','4','1555951419','1555951419','','N288413074313','E15559514136241','25.00');
INSERT INTO `ysk_userrob` VALUES('395','201','2','150.00','0.00','50000.00','13000000000','网络','838','3','1555953727','1555953727','1555953763','N703392553537','E1555953633122','150.00');
INSERT INTO `ysk_userrob` VALUES('396','201','2','369.00','0.00','49851.50','13000000000','网络','839','3','1555954035','1555954035','1555954051','N670429308739','E155595401486','369.00');
INSERT INTO `ysk_userrob` VALUES('397','202','2','20000.00','0.00','100000.00','15279770118','陈鹏','840','3','1555954774','1555954774','1555954788','N964680466455','E15559547696534','20000.00');
INSERT INTO `ysk_userrob` VALUES('398','202','2','78000.00','0.00','80200.00','15279770118','陈鹏','841','3','1555954884','1555954884','1555954896','N425941952735','E1555954878880','78000.00');
INSERT INTO `ysk_userrob` VALUES('399','202','2','50000.00','0.00','100000.00','15279770118','陈鹏','842','3','1555955005','1555955005','1555955020','N227110944312','E15559549983071','50000.00');
INSERT INTO `ysk_userrob` VALUES('400','201','2','50000.00','0.00','100000.00','13000000000','网络','843','3','1555956107','1555956107','1555956121','N651665066111','E15559561045865','50000.00');
INSERT INTO `ysk_userrob` VALUES('401','199','2','258.00','0.00','10025.00','18248654865','左刚','844','3','1555986208','1555986208','1555986236','N232594635679','E1555985779277','258.00');
INSERT INTO `ysk_userrob` VALUES('402','199','2','0.00','0.00','9769.58','18248654865','左刚','885','3','1555986291','1556003084','1556019399','N899539353844','E15559862747908','1055.00');
INSERT INTO `ysk_userrob` VALUES('403','199','2','88.00','0.00','9769.58','18248654865','左刚','890','3','1556019231','1556019231','1556019339','N178023315184','E15560156788727','88.00');
INSERT INTO `ysk_userrob` VALUES('404','199','2','99.00','0.00','9693.01','18248654865','左刚','897','3','1556031313','1556031313','1556031327','N377625148644','E15560312313440','99.00');
INSERT INTO `ysk_userrob` VALUES('405','199','2','258.00','0.00','9595.00','18248654865','左刚','898','3','1556031436','1556031436','1556031463','N178503460129','E15560314331707','258.00');
INSERT INTO `ysk_userrob` VALUES('406','199','2','253.00','0.00','9339.58','18248654865','左刚','899','4','1556031488','1556031488','','N805862542434','E15560314807078','253.00');
--
-- 表的结构ysk_withdraw
--
DROP TABLE IF EXISTS `ysk_withdraw`;
CREATE TABLE `ysk_withdraw` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` int(11) NOT NULL COMMENT '会员ID',
  `account` varchar(225) NOT NULL COMMENT '提现账号',
  `name` varchar(225) NOT NULL COMMENT '提现人姓名',
  `way` varchar(225) NOT NULL COMMENT '提现方式',
  `price` float(10,2) NOT NULL COMMENT '提现金额',
  `addtime` varchar(225) NOT NULL COMMENT '提现时间',
  `endtime` varchar(225) NOT NULL COMMENT '完成时间',
  `status` int(11) NOT NULL COMMENT '状态1提交，2退回3成功',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='提现申请表';

--
-- 转存表中的数据 ysk_withdraw

INSERT INTO `ysk_withdraw` VALUES('8','196','6214532588668','张三','建设银行','3000.00','1555735964','','3');
INSERT INTO `ysk_withdraw` VALUES('9','196','6214832582586','王王','建设银行','3000.00','1555736094','','3');
INSERT INTO `ysk_withdraw` VALUES('10','196','6214835762360137','左刚','招商银行','3000.00','1555749365','','3');
INSERT INTO `ysk_withdraw` VALUES('11','198','123456789','张飒','张三','2000.00','1555919458','','2');
